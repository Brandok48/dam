var express = require("express");

var cors = require('cors');
var db = require('mongoose')
var app = express();
var usuarioroutes = require('./routes/usuarioRoutes')
var rescateroutes = require('./routes/rescateRoutes')
var bodyParser = require("body-parser")
//var rescate = require('./models/rescate.js')


db.connect('mongodb://localhost/rescate', {useNewUrlParser: true});

app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())


app.use('/usuario',usuarioroutes)
app.use('/rescate',rescateroutes)


// CORS

app.use(cors());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});




app.get('/', function (req, res) {
    res.send('Saludos desde express');
  });
// app.get('/getRescates', function (req, res) {
//   var query = rescate.find({})
//   query.exec(function (err, docs) {res.send(docs)});

// });
app.get('/getUltimoRescate', function (req, res) {
  var query = rescate.find({}).limit(1).sort({$natural:-1})
  query.exec(function (err, docs) {res.send(docs)});

});




// app.post('/guardarUsuario', function (req, res) {
 
//   var res = new Usuario({
//     nombre : req.body.nombre,
//     apellidos: req.body.apellidos,
//     nick: req.body.nick,
//     pass: req.body.pass,
//   })
//   res.save(function (err) {
//     if (err) return handleError(err);
//   });
// });