var express = require('express')
var app = express()

var controller = require('../controllers/rescateController')

app.get('/getRescates',controller.getRescates);
app.post('/guardarRescate',controller.guardarRescate)
module.exports = app