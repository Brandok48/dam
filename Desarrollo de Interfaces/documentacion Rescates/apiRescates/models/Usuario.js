var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var usuarioSchema = new Schema({
  nombre:  String,
  apellidos: String,
  nick:   String,
  pass: String,
});

module.exports = mongoose.model('Usuario', usuarioSchema);