var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var rescateSchema = new Schema({
  x:  String, // String is shorthand for {type: String}
  y: String,
  nombre:   String,
  radio: String,
  descripcion: String
});

module.exports = mongoose.model('rescate', rescateSchema);