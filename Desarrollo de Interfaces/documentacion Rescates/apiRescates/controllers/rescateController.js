var rescate = require('../models/rescate.js')

function getRescates(req,res){
    var query = rescate.find({})
    query.exec(function (err, docs) {res.send(docs)});
}
function guardarRescate(req,res){

    var res = new rescate({
        nombre : req.body.nombre,
        x: req.body.coordX+"",
        y: req.body.coordY+"",
        radio: req.body.radio/2+"",
        descripcion: req.body.descripcion
      })
      res.save(function (err) {
        if (err) return handleError(err);
      });
}
module.exports = {
    getRescates,
    guardarRescate
}