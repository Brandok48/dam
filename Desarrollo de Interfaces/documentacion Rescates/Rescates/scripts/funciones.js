nuevalerta = false
const axios = require('axios');
var circle,circle1
var rescates
var marker
var rescate1
const {ipcRenderer} = require('electron') 

async function getRescates() {
  try {
    const response = await axios.get('http://localhost:3000/rescate/getRescates');
    
    rescates = response.data
  } catch (error) {
    console.error(error);
  }

  rescates.forEach(rescate => {
    
    if(marker1!=undefined)
      map.removeLayer(marker1)
      if(marker!=undefined)
      map.removeLayer(marker)
    var marker1 = L.marker([rescate.x, rescate.y],{title:rescate.nombre}).addTo(map);
    marker1.bindPopup("<b>Nombre: </b>"+rescate.nombre+"<br><b>Descripcion: </b>"+rescate.descripcion+"<br><b>Radio: </b>"+rescate.radio)
    marker1.on('mouseover',function(){
      
      
      if(circle!=undefined)
      map.removeLayer(circle)
      
    
      circle = L.circle([rescate.x, rescate.y], {
        color: 'green',
        fillColor: 'blue',
        fillOpacity: 0.3,
        radius: rescate.radio*1000
        }).addTo(map)
    })
    marker1.on('click',function(){
     

      if(circle1!=undefined)
      map.removeLayer(circle1)

      circle1 = L.circle([rescate.x, rescate.y], {
        color: 'yellow',
        fillColor: 'green',
        fillOpacity: 0.3,
        radius: rescate.radio*1000
        }).addTo(map)
    })
    marker1.on('mouseout',function(){
      
      if(circle!=undefined)
      map.removeLayer(circle)
    })
  });
}

  var radio = document.getElementById("radio")
  var rango = document.getElementById("myRange")
  radio.innerHTML = rango.value/2+"km";
  rango.oninput = function(){
    radio.innerHTML =this.value/2+"km"
    if(circle!=undefined)
    map.removeLayer(circle)

    circle = L.circle([x, y], {
      color: 'red',
      fillColor: 'yellow',
      fillOpacity: 0.3,
      radius: this.value*1000/2
      }).addTo(map)

      
  }


function onMapClick(e) {
  
  if(formAlerta.style.visibility=='visible'){
    ocultarForm(true,'formAlerta')
  }
  if(circle1!=undefined)
  map.removeLayer(circle1)
    if (nuevalerta){
      if(circle!=undefined)
      map.removeLayer(circle)
     
    coor = e.latlng;

    x = coor.lat
    y = coor.lng
    marker = L.marker([x, y],{title:document.getElementById('nombre').value}).addTo(map);
    marker.on('mouseover',function(){
      
      
      if(circle!=undefined)
      map.removeLayer(circle)
    
      circle = L.circle([x, y], {
        color: 'green',
        fillColor: 'blue',
        fillOpacity: 0.3,
        radius: document.getElementById('myRange').value*1000/2
        }).addTo(map)
    })
    marker.on('click',function(){
      

      if(circle1!=undefined)
      map.removeLayer(circle1)

      circle1 = L.circle([x, y], {
        color: 'yellow',
        fillColor: 'green',
        fillOpacity: 0.3,
        radius: document.getElementById('myRange').value*1000/2
        }).addTo(map)
    })
    marker.on('mouseout',function(){
     
      if(circle!=undefined)
      map.removeLayer(circle)
    })
    
   circle = L.circle([x, y], {
    color: 'red',
    fillColor: 'yellow',
    fillOpacity: 0.3,
    radius: 25*1000/2
    }).addTo(map)
    map.off('mousemove', null);
    document.getElementById("coords").innerHTML = ""
    document.getElementById("formAlerta").style.top = event.clientY-55+"px"
    document.getElementById("formAlerta").style.left = event.clientX+25+"px"
    document.getElementById("x").value = x;
    document.getElementById("y").value = y;
   
    document.getElementById('nombre').value = ''
    document.getElementById('descripcion').value = ''
    document.getElementById('myRange').value = 25
    document.getElementById("formAlerta").style.visibility = "visible"
    nuevalerta = false
    }
    
    document.getElementById("mapid").style.cursor = null;
    
}
function moverRaton(e) {
  document.getElementById("coords").innerHTML = "X: "+e.latlng.lat+"<br>Y: "+e.latlng.lng
  document.getElementById("coords").style.top = event.clientY+25+"px"
  document.getElementById("coords").style.left = event.clientX+25+"px"
}

function cambiarTamano(){
     
    document.getElementById('mapid').style.height=window.innerHeight-20+"px"
  }

function moverBoton(boton,x,y){
    if(x<0){
        document.getElementById(boton).style.right=window.innerWidth+1*x+"px"
    }else{
        document.getElementById(boton).style.left=window.innerWidth-x+"px"}
  
  document.getElementById(boton).style.top=window.innerHeight-1*y+"px"
}
function crearAlerta(){
  document.getElementById("mapid").style.cursor = "cell";
    nuevalerta = true
    map.on('mousemove', moverRaton);
}
function ocultarForm(bool,element){
  document.getElementById(element).style.visibility = 'hidden'
  formAlerta.style.top=0
    formAlerta.style.left=0
  if (bool){
    map.removeLayer(circle)
    map.removeLayer(marker)
  }



}
function crearUsuario(){
  document.getElementById('formCrearUsuario').style.visibility = 'visible'
}
function confirmar(){
  ipcRenderer.send('nuevaAlertaClick','hola')
}
function recargar(){
  ipcRenderer.send('recargar','hola')
}