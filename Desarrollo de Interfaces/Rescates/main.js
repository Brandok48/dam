const { app, BrowserWindow, ipcMain } = require('electron')

let win1
// Mantén una referencia global del objeto window, si no lo haces, la ventana 
// se cerrará automáticamente cuando el objeto JavaScript sea eliminado por el recolector de basura.
let win
const electron = require('electron')

// Enable live reload for Electron too
require('electron-reload')(__dirname, {
    // Note that the path to electron may vary according to the main file
    electron: require(`${__dirname}/node_modules/electron`)
});
function createWindow () {
  // Crea la ventana del navegador.
  win = new BrowserWindow({
    width: 1000,
    height: 800,
    webPreferences: {
      nodeIntegration: true
    }
    
  })
  win.setMenuBarVisibility(false)
  // y carga el index.html de la aplicación.
  win.loadFile('index.html')

  // Abre las herramientas de desarrollo (DevTools).
   // win.webContents.openDevTools()

  // Emitido cuando la ventana es cerrada.
  win.on('closed', () => {
    // Elimina la referencia al objeto window, normalmente  guardarías las ventanas
    // en un vector si tu aplicación soporta múltiples ventanas, este es el momento
    // en el que deberías borrar el elemento correspondiente.
    win = null
  })
}

// Este método será llamado cuando Electron haya terminado
// la inicialización y esté listo para crear ventanas del navegador.
// Algunas APIs pueden usarse sólo después de que este evento ocurra.
app.on('ready', createWindow)

// Sal cuando todas las ventanas hayan sido cerradas.
app.on('window-all-closed', () => {
  // En macOS es común para las aplicaciones y sus barras de menú
  // que estén activas hasta que el usuario salga explicitamente con Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // En macOS es común volver a crear una ventana en la aplicación cuando el
  // icono del dock es clicado y no hay otras ventanas abiertas.
  if (win === null) {
    createWindow()
  }
})


// En este archivo puedes incluir el resto del código del proceso principal de
// tu aplicación. También puedes ponerlos en archivos separados y requerirlos aquí.


ipcMain.on('nuevaAlertaClick',(event,coords)=>{
  //mainWindow.loadFile('crearAlerta.html');
 win1 = new BrowserWindow({ width: 400,
     height: 250,
     webPreferences: {
      nodeIntegration: true
    }})
  win1.loadFile('confirmarAlerta.html');
  win1.setMenuBarVisibility(false)
  win1.show();
  //win.reload();
  
});
ipcMain.on('chat',(event,idRescate)=>{
  //mainWindow.loadFile('crearAlerta.html');
 ventanaChat = new BrowserWindow({ width: 400,
     height: 250,
     webPreferences: {
      nodeIntegration: true
    }})
    ventanaChat.webContents.on('did-finish-load',(args)=>{
      ventanaChat.webContents.send('idRescate',idRescate)
    })
    
    ventanaChat.loadFile('chat.html');
    ventanaChat.setMenuBarVisibility(false)
    ventanaChat.show();
  //win.reload();
  
});
ipcMain.on('recargar',(event,coords)=>{
 
  win.reload();
  win1.close()
  
});
