nuevalerta = false
const axios = require('axios');
var socket
var circle,circle1
var rescates
var horas
var tracks
var marker
var polyline
var polyLinePoints
var rescate1
const {ipcRenderer} = require('electron') 


async function guardarRescate(){
    //console.log("aaaaaaaaaa")
    const response = await axios.post('http://127.0.0.1:3000/rescate/guardarRescate',{})
   // console.log(response.data)
    
}
function abrirChat(idGrupo){
  ipcRenderer.send('chat',idGrupo)
  
 

}
async function getTracks(idGrupo){
  try {
    const response = await axios.get('http://localhost:3000/track/getTracks/'+idGrupo);
    
    tracks = response.data.json
    
    tracks.forEach(track => {
     // console.log(track)
      polyLinePoints = []
      track.coord.forEach(coord => {
       // console.log(coord)
        polyLinePoints.push([coord.x,coord.y])
      });
     // console.log(polyLinePoints)
      polyline = L.polyline(polyLinePoints,{color: 'purple'}).addTo(map)
    });
    
  } catch (error) {
    console.error(error);
  }
}
async function getHoras(idRescate){
  try {
    const hora = await axios.get('http://localhost:3000/rescate/contarHoras/'+idRescate);
    
    horas = hora.data
   
   return horas
  } catch (error) {
    console.error(error);
  }
}
async function getIntegrantes(idGrupo){
  try {
    const integ = await axios.get('http://localhost:3000/usuario/getNumVoluntarios/'+idGrupo);
    
    var integrantes = integ.data
   
   return integrantes
  } catch (error) {
    console.error(error);
  }
}
async function getRescates() {
  try {
    const response = await axios.get('http://localhost:3000/rescate/getRescates');
    
    rescates = response.data
   
  } catch (error) {
    console.error(error);
  }

  rescates.forEach(rescate => {
    
    
    if(marker1!=undefined)
      map.removeLayer(marker1)
      if(marker!=undefined)
      map.removeLayer(marker)
    var marker1 = L.marker([rescate.x, rescate.y],{title:rescate.nombre}).addTo(map);
    marker1.bindPopup("<b>Nombre: </b>"+rescate.nombre+"<br><b>Descripcion: </b>"+rescate.descripcion+"<br><b>Radio: </b>"+rescate.radio)
    marker1.on('mouseover',function(){
      
      
      if(circle!=undefined)
      map.removeLayer(circle)
      
    
      circle = L.circle([rescate.x, rescate.y], {
        color: 'green',
        fillColor: 'blue',
        fillOpacity: 0.3,
        radius: rescate.radio*1000
        }).addTo(map)
    })
    marker1.on('click',function(){
      getTracks(rescate.idGrupo)
      document.getElementById("formAlertaMod").style.top = event.clientY-55+"px"
      document.getElementById("formAlertaMod").style.left = event.clientX+25+"px"
      document.getElementById("modx").value = rescate.x;
      document.getElementById("mody").value = rescate.y;
      //console.log(getHoras(rescate._id))
      var prom = getHoras(rescate._id)
      var h
      prom.then(element=>{
        
        h = element
        duracion.innerHTML = h
      })
      var prom = getIntegrantes(rescate.idGrupo)
      var i
      prom.then(element=>{
        
        i = element
        integrantes.innerHTML = i
      })
      idRescate.value = rescate._id
      idGrupo.value = rescate.idGrupo
      
      document.getElementById('modnombre').value = rescate.nombre
      document.getElementById('moddescripcion').value = rescate.descripcion
      document.getElementById('myRange').value = rescate.radio
      document.getElementById("formAlertaMod").style.visibility = 'visible'
      //activarChat(rescate._id,rescate.nombre)
      if(circle1!=undefined)
      map.removeLayer(circle1)

      circle1 = L.circle([rescate.x, rescate.y], {
        color: 'yellow',
        fillColor: 'green',
        fillOpacity: 0.3,
        radius: rescate.radio*1000
        }).addTo(map)
        socket = require('socket.io-client')('http://localhost:4000');
        socket.on('actualizar',function(track){
          if(track.idGrupo==rescate.idGrupo)
         // console.log(track)
          var points = [[track.anterior.x,track.anterior.y],[track.posterior.x,track.posterior.y]]
          L.polyline(points,{color: 'orange'}).addTo(map)
          })
    })
    marker1.on('mouseout',function(){
      
      if(circle!=undefined)
      map.removeLayer(circle)
    })
  });
}

 
function activarChat(id,nombre){
    document.getElementById('nombreChat').innerHTML ='Rescate de '+nombre +"<input type='text' name='idGrupo' value='"+id+"' style='visibility:hidden' readonly><button onCLick>"
    document.getElementById('chat').style.visibility = 'visible'
}

function onMapClick(e) {
  document.getElementById('listaUSuarios').style.visibility = 'hidden'
  
  if(formAlerta.style.visibility=='visible'){
    ocultarForm(true,'formAlerta')
  }
  if(formAlertaMod.style.visibility=='visible'){
    ocultarForm(true,'formAlertaMod')
  }
  if(socket!=undefined)
    if(socket.connected)
    socket.disconnect()
  // if(circle1!=undefined)
  // map.removeLayer(circle1)
  // if(polyline!=undefined){
    for(i in map._layers) {
      if(map._layers[i]._path != undefined) {
          try {
              map.removeLayer(map._layers[i]);
          }
          catch(e) {
              console.log("problem with " + e + map._layers[i]);
          }
      }
  //}
  }
  //map.removeLayer(polyline)

    if (nuevalerta){
      if(circle!=undefined)
      map.removeLayer(circle)
     
    coor = e.latlng;

    x = coor.lat
    y = coor.lng
    marker = L.marker([x, y],{title:document.getElementById('nombre').value}).addTo(map);
    marker.on('mouseover',function(){
      
      
      if(circle!=undefined)
      map.removeLayer(circle)
    
      circle = L.circle([x, y], {
        color: 'green',
        fillColor: 'blue',
        fillOpacity: 0.3,
        radius: document.getElementById('myRange').value*1000/2
        }).addTo(map)
    })
    marker.on('click',function(){
     
      if(circle1!=undefined)
      map.removeLayer(circle1)

      circle1 = L.circle([x, y], {
        color: 'yellow',
        fillColor: 'green',
        fillOpacity: 0.3,
        radius: document.getElementById('myRange').value*1000/2
        }).addTo(map)
    })
    marker.on('mouseout',function(){
     
      if(circle!=undefined)
      map.removeLayer(circle)
    })
    
   circle = L.circle([x, y], {
    color: 'red',
    fillColor: 'yellow',
    fillOpacity: 0.3,
    radius: 25*1000/2
    }).addTo(map)
    map.off('mousemove', null);
    document.getElementById("coords").innerHTML = ""
    document.getElementById("formAlerta").style.top = event.clientY-55+"px"
    document.getElementById("formAlerta").style.left = event.clientX+25+"px"
    document.getElementById("x").value = x;
    document.getElementById("y").value = y;
   
    document.getElementById('nombre').value = ''
    document.getElementById('descripcion').value = ''
    document.getElementById('myRange').value = 25
    document.getElementById("formAlerta").style.visibility = "visible"
    nuevalerta = false
    }
    
    document.getElementById("mapid").style.cursor = null;
    
}
function moverRaton(e) {
  document.getElementById("coords").innerHTML = "X: "+e.latlng.lat+"<br>Y: "+e.latlng.lng
  document.getElementById("coords").style.top = event.clientY+25+"px"
  document.getElementById("coords").style.left = event.clientX+25+"px"
}

function cambiarTamano(){
     
    document.getElementById('mapid').style.height=window.innerHeight-20+"px"
  }

function moverBoton(boton,x,y){
    if(x<0){
        document.getElementById(boton).style.right=window.innerWidth+1*x+"px"
    }else{
        document.getElementById(boton).style.left=window.innerWidth-x+"px"}
  
  document.getElementById(boton).style.top=window.innerHeight-1*y+"px"
}
function crearAlerta(){
  document.getElementById("mapid").style.cursor = "cell";
    nuevalerta = true
    map.on('mousemove', moverRaton);
}
function ocultarForm(bool,element){
  document.getElementById(element).style.visibility = 'hidden'
    formAlerta.style.top=0
    formAlerta.style.left=0
    formAlertaMod.style.top=0
    formAlertaMod.style.left=0
  if (bool){
    map.removeLayer(circle)
    map.removeLayer(marker)
  }



}
async function eliminarAlerta(idAlerta){
  try {
    const response = await axios.post('http://localhost:3000/rescate/eliminarRescate/',{
      idRescate:idAlerta
    });
    
    rescates = response.data
  } catch (error) {
    console.error(error);
  }
}
function crearUsuario(){
  document.getElementById('formCrearUsuario').style.visibility = 'visible'
}
async function verUsuarios(){
  try {
    const response = await axios.get('http://localhost:3000/usuario/getUsuarios');
    
    usuarios = response.data
    
    usuarios.forEach(element => {
      var node = document.createElement("LI");
   
      var textnode = document.createTextNode(element.nick+': '+element.nombre);
      //console.log(textnode)
      node.appendChild(textnode);
      //node.appendChild(but)
      //console.log(node)
      document.getElementById('usus').appendChild(node);
      //document.getElementById('listaUSuarios').appendChild(but);
      
  });
    
  } catch (error) {
    console.error(error);
  }
  document.getElementById('listaUSuarios').style.visibility = 'visible'
}
function confirmar(){
  ipcRenderer.send('nuevaAlertaClick','hola')
}
function recargar(){
  ipcRenderer.send('recargar','hola')
}