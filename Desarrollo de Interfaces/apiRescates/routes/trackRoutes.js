var express = require('express')
var app = express()

var controller = require('../controllers/trackController')

app.get('/getTracks/:idGrupo',controller.getTracks)
app.get('/getTracksUsu/:nick',controller.getTracksUSu)
//app.post('/adminMensaje',controller.adminMensaje)
module.exports = app