var express = require('express')
var app = express()

var controller = require('../controllers/grupoController')

app.post('/crearGrupo',controller.guardarGrupo);
app.post('/anadirUsu',controller.anadirUsuario)
app.post('/quitarUsu',controller.quitarUsuario)

module.exports = app