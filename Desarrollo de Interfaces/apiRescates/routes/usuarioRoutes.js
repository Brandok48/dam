var express = require('express')
var app = express()

var controller = require('../controllers/usuarioController')

app.get('/getUsuario/:nick',controller.buscarUsuario)
app.get('/getUsuarios',controller.buscarUsuarios);
app.get('/getNumVoluntarios/:idGrupo',controller.getNumeroVoluntarios);
app.post('/login',controller.login);
app.post('/guardarUsuario',controller.guardarUsuario);
app.post('/asignarGrupo',controller.asignarGrupo);
app.get('/asignarGrupo/:nick/:idGrupo',controller.asignarGrupo);
app.get('/desAsignarGrupo/:nick',controller.desAsignarGrupo);
app.post('/desAsignarGrupo',controller.desAsignarGrupo);

module.exports = app