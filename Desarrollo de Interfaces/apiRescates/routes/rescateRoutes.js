var express = require('express')
var app = express()

var controller = require('../controllers/rescateController')

app.get('/getRescates',controller.getRescates);
app.get('/contarHoras/:idRescate',controller.contarHoras);
app.get('/getRescate/:idRescate',controller.getRescate);
app.get('/getRescateGrupo/:idGrupo',controller.getRescateGrupo);//coge el rescate a partir del  id del grupo
app.post('/guardarRescate',controller.guardarRescate)
app.post('/asignarGrupo',controller.asignarGrupo)
app.post('/modificarRescate',controller.modificarRescate)
app.post('/eliminarRescate',controller.eliminarRescate)

module.exports = app