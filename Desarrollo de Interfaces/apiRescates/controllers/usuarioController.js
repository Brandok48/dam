var Usuario = require('../models/usuario');


function guardarUsuario(req,res){
    //res.status(200).json('success');
    //console.log(Usuario.findOne({nick:req.body.nick}).exec(function(err,docs){res.send(docs)}))
    
    Usuario.findOne({nick:req.body.nick}).exec(function (err, docs) {

      if(docs==null){
          var res1 = new Usuario({
              nombre : req.body.nombre,
              apellidos: req.body.apellidos,
              nick: req.body.nick,
              pass: req.body.pass,
              idGrupo: "no asignado"
            })
          //
            res1.save(function (err) {
              if (err) return handleError(err);
            });
          }else{
            //console.log('ese usuario ya existe')
            //res.status(400).json()
            
          }

    })
    
    
}
function buscarUsuario(req,res){
  //res.status(200).json('success');
  //console.log(Usuario.findOne({nick:req.body.nick}).exec(function(err,docs){res.send(docs)}))
  
  Usuario.findOne({nick:req.params.nick}).exec(function (err, docs) {

    if(docs==null){
        console.log('ese usuario no existe')
        }else{
          res.status(200).json({
            ok:true,
            docs:docs
          })
          
        }

  })
  
  
}


function buscarUsuarios(req,res){
    var query = Usuario.find({})
    query.exec(function (err, docs) {res.send(docs)});
}


function login(req,res){
  var query = Usuario.findOne({nick:req.body.nick})
  query.exec(function (err, docs) {
    if(req.body.pass == docs.pass){
      res.status(200).json({
        ok:true,
        docs
      })
     
    }
    else{
      res.status(400).json({
        ok:false,
        mensaje:'credenciales incorrectas'
      })
    }
  });
}
function asignarGrupo(req,res){
  var query = Usuario.findOne({nick:req.params.nick})
  query.exec(function (err, docs) {
    docs.idGrupo = req.params.idGrupo+""
    docs.save()
    res.status(200).json({
      true:"ok",
      idGrupo:req.params.idGrupo
    })
  });
}
function desAsignarGrupo(req,res){
  var query = Usuario.findOne({nick:req.params.nick})
  query.exec(function (err, docs) {
    docs.idGrupo = "no asignado"
    docs.save()
  });
}
function getNumeroVoluntarios(req,res){
  Usuario.count({ "idGrupo": req.params.idGrupo }, function (err, count) {
    res.status(200).json(count)
    //console.log('hay %d voluntarios', count);
  })
  }
module.exports = {
    guardarUsuario,
    buscarUsuarios,
    login,
    asignarGrupo,
    desAsignarGrupo,
    buscarUsuario,
    getNumeroVoluntarios
}