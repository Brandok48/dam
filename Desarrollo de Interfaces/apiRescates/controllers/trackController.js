var Track = require('../models/Track');

function guardarTrack(jsonCoord){

    
    Track.findOne({"nick":jsonCoord.nick,"idGrupo":jsonCoord.idGrupo}).exec(function (err, json){

        tr = {
            x:jsonCoord.posterior.x,
            y:jsonCoord.posterior.y,
        }
        //console.log(json.coord[0])
        //console.log(json.coord.length)
        if(json!=undefined&&json.nick==jsonCoord.nick){
            if(tr.x!=json.coord[json.coord.length-1].x&&tr.y!=json.coord[json.coord.length-1].y){
                console.log(json.nick+"\n"+json.idGrupo)
                json.coord.push(tr)
                json.save()
            }
            
        }else{
            var men = new Track({
                coord:tr,
                nick:jsonCoord.nick,
                idGrupo:jsonCoord.idGrupo
            })
            men.save(function (err) {
                if (err) return handleError(err);
              });
        }
    })
   
}
function getTracks(req,res) {
    Track.find({"idGrupo":req.params.idGrupo}).exec(function (err, json){
        if(json==undefined){
       res.status(400).json({
           ok:false,
           message:"no se han encontrado tracks para ese grupo"
       })
        }else{
            res.status(200).json({
                ok:true,
                json:json
            })
        }
    //console.log(json)
    })
}
function getTracksUSu(req,res) {
    Track.find({"nick":req.params.nick}).exec(function (err, json){
        if(json==undefined){
       res.status(400).json({
           ok:false,
           message:"no se han encontrado tracks para ese usuario"
       })
        }else{
            res.status(200).json({
                ok:true,
                json:json
            })
        }
    //console.log(json)
    })
}
function handleError(err) {
   console.log(err)
  }

module.exports = {
    guardarTrack,
    getTracks,
    getTracksUSu
    
}