var rescate = require('../models/rescate.js')

function getRescates(req,res){
    var query = rescate.find({}).sort({fechaCreacion:-1})
    query.exec(function (err, docs) {
     // console.log(docs)
      res.send(docs)});
}
function getRescate(req,res){
  //console.log(req.params)
  rescate.findOne({"idGrupo":req.params.idRescate}).exec(function(err,json){
    res.send(json)
})
}
function getRescateGrupo(req,res){
  //console.log(req.params)
  rescate.findOne({"idGrupo":req.params.idGrupo}).exec(function(err,json){
    //console.log(json)
    res.send(json)
})
}
function guardarRescate(req,res){
//console.log(req.body);
    var res1 = new rescate({
        nombre : req.body.nombre,
        x: req.body.coordX+"",
        y: req.body.coordY+"",
        fechaCreacion:new Date(),
        radio: req.body.radio/2+"",
        descripcion: req.body.descripcion,
        idGrupo:Math.floor(Math.random()*1000)
      })
      res1.save(function (err) {
        if (err) return handleError(err);
      });
      // res.status(200).json({
      //   ok:true,
      //   notificacion:"Se manda uns notificacion de que se ha creado una alerta"
      // })
}
function modificarRescate(req,res){
 
  rescate.findOne({"_id":req.body.idRescate}).exec(function(err,json){

   // console.log(json)
    json.x = req.body.coordX
    json.nombre = req.body.nombre
    json.y = req.body.coordY
    json.radio= req.body.radio/2+""
    json.idGrupo = req.body.idGrupo
    json.descripcion= req.body.descripcion
    //console.log(json)
    json.save()
})
  
}
function eliminarRescate(req,res){
  rescate.findOne({"_id":req.body.idRescate}).exec(function(err,json){
    
    json.remove()
})
}

function asignarGrupo(req,res){
  rescate.findOne({"_id":req.body.idRescate}).exec(function(err,json){
    json.idGrupo = req.body.idGrupo
    json.save()
})
}
function contarHoras(req,res){
  var fecha = new Date();
  rescate.findOne({"_id":req.params.idRescate}).exec(function(err,json){
    res.status(200).json(msToTime(fecha-json.fechaCreacion))
})
}
function msToTime(duration) {
  var milliseconds = parseInt((duration % 1000) / 100),
    seconds = Math.floor((duration / 1000) % 60),
    minutes = Math.floor((duration / (1000 * 60)) % 60),
    hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

  hours = (hours < 10) ? "0" + hours : hours;
  minutes = (minutes < 10) ? "0" + minutes : minutes;
  seconds = (seconds < 10) ? "0" + seconds : seconds;

  return hours + ":" + minutes + ":" + seconds;
}


module.exports = {
    getRescates,
    getRescate,
    getRescateGrupo,
    guardarRescate,
    asignarGrupo,
    modificarRescate,
    eliminarRescate,
    contarHoras
}