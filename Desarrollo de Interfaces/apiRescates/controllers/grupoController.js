var Grupo = require('../models/Grupo')

function guardarGrupo(req,res){

    var res = new Grupo({
        coord:{
            x:req.body.x,
            y:req.body.y,
            radio:req.body.radio
        },
        idUsuario:[],
      })
      res.save(function (err) {
        if (err) return handleError(err);
      });
}
function anadirUsuario(req,res){
    
    Grupo.findOne({"_id":req.body.idGrupo}).exec(function(err,json){
        json.idUsuario.push({"_id":req.body.nick})
        json.save()
    })
}
function quitarUsuario(req,res){
    
    Grupo.findOne({"_id":req.body.idGrupo}).exec(function(err,json){
        
        if(json.idUsuario.includes(req.body.nick)){ 
            json.idUsuario.pull(req.body.nick);
            json.save()
        }
        
    })
}
module.exports = {
    guardarGrupo,
    anadirUsuario,
    quitarUsuario
}