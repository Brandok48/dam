var Mensaje = require('../models/Mensaje');
//var Usuario = require('../models/Usuario');
function getMensajes(req,res) {
    Mensaje.findOne({"idGrupo":req.params.idGrupo}).exec(function (err, json){
        
        if(json==undefined){
       res.status(400).json({
           ok:false,
           message:"no se han encontrado mensajes para ese grupo"
       })
        }else{
            res.status(200).json({
                ok:true,
                json:json.mensaje
            })
        }
    })
}
function guardarMensaje(senderNickname,messageContent,idUsuario,idGrupo){

    d = new Date();
    h = d.getHours();
    m = d.getMinutes();
    if(h<10)
    h = '0'+h
    if(m<10)
    m = '0'+m
    date = h+':'+m
    mensa = {
        idUsuario:idUsuario,
        mensaje:messageContent,
        fecha : date,
        nickUsuario:senderNickname
    }
//
    //create a message object 
    Mensaje.findOne({"idGrupo":idGrupo}).exec(function (err, json){
        if(json!=undefined){
        json.mensaje.push(mensa)
        json.save()
        }else{
            var men = new Mensaje({
                mensaje:mensa,
                idGrupo:idGrupo
            })
            men.save(function (err) {
                if (err) return handleError(err);
              });
        }
    })
   
}

module.exports = {
    guardarMensaje,
    getMensajes
}