var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var rescateSchema = new Schema({
  x:  String, // String is shorthand for {type: String}
  y: String,
  fechaCreacion: Date,
  nombre:   String,
  radio: String,
  descripcion: String,
  idGrupo:String
});

module.exports = mongoose.model('rescate', rescateSchema);