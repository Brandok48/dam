var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var trackSchema = new Schema({
  coord: [JSON],
  nick:   String,
  idGrupo: String,
});

module.exports = mongoose.model('Track', trackSchema);