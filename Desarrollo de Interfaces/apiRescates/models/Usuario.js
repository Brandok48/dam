var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var usuarioSchema = new Schema({
  nombre:  String,
  apellidos: String,
  nick:   String,
  pass: String,
  idGrupo: String,
});

module.exports = mongoose.model('Usuario', usuarioSchema);