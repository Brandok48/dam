var express = require("express");

var cors = require('cors');
var db = require('mongoose')
var app = express();
var usuarioroutes = require('./routes/usuarioRoutes')
var rescateroutes = require('./routes/rescateRoutes')
var grupoRoutes = require('./routes/grupoRoutes')
var mensajeRoutes = require('./routes/mensajeRoutes')
var trackRoutes = require('./routes/trackRoutes')
var bodyParser = require("body-parser")
var Mensaje = require('./controllers/mensajeController')
var Track = require('./controllers/trackController')
var Grupo = require('./controllers/grupoController')

//var rescate = require('./models/rescate.js')

http = require('http'),
  app = express(),
  server = http.createServer(app),
  io = require('socket.io').listen(server);
app.get('/', (req, res) => {

  res.send('Chat Server is running on port 3000')
});
io.on('connection', (socket) => {

  console.log('user connected')

  socket.on('join', function (userNickname) {

    console.log(userNickname + " : has joined the chat ");
   // Grupo.anadirUsuario("5e283fb732ebc31240e14138",'pepe')

    socket.broadcast.emit('userjoinedthechat', userNickname + " : has joined the chat ");
  })

  socket.on('messagedetection', (senderNickname, messageContent,idUsuario,idGrupo) => {

    //log the message in console 
//console.log(senderNickname)
    // console.log(senderNickname+" : " +messageContent)
    Mensaje.guardarMensaje(senderNickname,messageContent,idUsuario,idGrupo)
    
    let message = { "message": messageContent, "senderNickname": senderNickname + ": " ,"idGrupo":idGrupo}

    // send the message to all users including the sender  using io.emit() 
    //console.log(idGrupo)
    io.emit("message", message)

  })
  socket.on('sendTrack',function(coord){
    //console.log(coord)
    Track.guardarTrack(coord)
    socket.broadcast.emit('actualizar',coord)
  })
  socket.on('disconnect', function () {

    console.log('user has left ')

    socket.broadcast.emit("userdisconnect", ' user has left')

  })

})


server.listen(4000, () => {

  console.log('Node app is running on port 4000')

})

db.connect('mongodb://localhost/rescate', { useNewUrlParser: true });

app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())


app.use('/usuario', usuarioroutes)
app.use('/rescate', rescateroutes)
app.use('/grupo', grupoRoutes)
app.use('/mensaje', mensajeRoutes)
app.use('/track',trackRoutes)

// CORS

app.use(cors());
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.listen(3000, () => {
  console.log("El servidor está inicializado en el puerto 3000");
});

app.get('/getUltimoRescate', function (req, res) {
  var query = rescate.find({}).limit(1).sort({ $natural: -1 })
  query.exec(function (err, docs) { res.send(docs) });

});
