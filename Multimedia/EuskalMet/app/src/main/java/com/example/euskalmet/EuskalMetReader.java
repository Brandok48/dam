package com.example.euskalmet;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class EuskalMetReader extends AsyncTask<String,Void,String> {
    InputStream in;

    Handler main;

    public EuskalMetReader(Handler main){

    this.main = main;
    }
    @Override
    protected String doInBackground(String... strings) {
        URL url;
        HttpURLConnection urlConnection = null;
        try {
            url = new URL("http://www.euskalmet.euskadi.eus/contenidos/prevision_tiempo/met_forecast/opendata/met_forecast.xml");
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream());

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            //urlConnection.disconnect();
        }

    try {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new InputStreamReader(in,"ISO-8859-1"));
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {

           if (eventType == XmlPullParser.START_TAG && xpp.getName().equals("es")) {
                 xpp.next();

                 enviar(xpp.getText());
            }
            eventType = xpp.next();
        }
    }catch (XmlPullParserException | IOException e){
        e.printStackTrace();
    }
        return null;
    }


    public void enviar(String s){
       final String st = s;

        main.post(new Runnable() {
            @Override
            public void run() {

                Message msg = main.obtainMessage();
                Bundle bundle = new Bundle();
                bundle.putString("Mensaje", st);
                msg.setData(bundle);
                main.sendMessage(msg);

            }
        });
    }
}
