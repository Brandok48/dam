package com.example.socketiotest;

import android.location.Location;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.json.JSONArray;

public class ViewModelCompartido extends ViewModel {


    private String[] datos;
    public void setDatos(String[] datos){
        this.datos = datos;
    }


    public String[] getDatos() {
        return datos;
    }

    private final MutableLiveData<Location> selected = new MutableLiveData<Location>();

    public void selectLocation(Location item) {
        selected.setValue(item);
    }

    public LiveData<Location> getSelectedLocation() {
        return selected;
    }


}
