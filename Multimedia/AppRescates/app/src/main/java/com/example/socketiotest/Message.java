package com.example.socketiotest;

public class Message {

    private String nickname;
    private String message ;
    private String idGrupo;

    public  Message(){


    }
    public Message(String nickname, String message,String idGrupo) {
        this.nickname = nickname;
        this.message = message;
        this.idGrupo = idGrupo;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIdGrupo() {
        return idGrupo;
    }

    public void setIdGrupo(String idGrupo) {
        this.idGrupo = idGrupo;
    }
}