package com.example.socketiotest;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;



import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {


    private Button btn;
    private EditText nickname, password;

    public static final String NICKNAME = "usernickname";
    //String url = "http://192.168.137.1:3000/usuario/login";
   // String url = "http://10.18.48.124:3000/usuario/login";
   // String url = "http://10.0.2.2:3000/usuario/login";
   // String url = "http://10.18.49.54:3000/usuario/login";
    String ip = "http://10.18.48.124";
    public String rescates;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //call UI components  by id
        btn = (Button) findViewById(R.id.enterchat);
        nickname = (EditText) findViewById(R.id.nickname);
        password = (EditText) findViewById(R.id.password);


        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if the nickname is not empty go to chatbox activity and add the nickname to the intent extra



                if (!nickname.getText().toString().isEmpty()) {
                    login(nickname.getText().toString(),password.getText().toString());
                }
            }
        });

    }
    public void login(final String usuario,final String contrasena){

        StringRequest postRequest = new StringRequest(Request.Method.POST, ip+":3000/usuario/login",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) { // Si el usuario es correcto me lleva a la nueva actividad


                        String grupo = "",docs;
                        try {
                            JSONObject json = new JSONObject(response);
                            docs = json.getString("docs");
                            JSONObject usuario = new JSONObject(docs);
                            grupo = usuario.getString("idGrupo");
                            Log.e("mainActivity", grupo );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(grupo.equalsIgnoreCase("no asignado")) {
                            Intent myIntent = new Intent(MainActivity.this, ListaRescates.class);
                            myIntent.putExtra("usuario", response);
                            //Log.e("main",rescates);
                            //myIntent.putExtra("rescates", rescates);
                            myIntent.putExtra("ip", ip);
                            myIntent.putExtra(NICKNAME, nickname.getText().toString());
                            startActivity(myIntent);
                        }else{
                            Intent myIntent = new Intent(MainActivity.this, ChatBoxActivity.class);
                            myIntent.putExtra("usuario", response);
                            try {
                                JSONObject idGrupo = new JSONObject(response);
                                myIntent.putExtra("idGrupo",grupo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                           // myIntent.putExtra("rescates", rescates);
                            myIntent.putExtra("ip", ip);
                            myIntent.putExtra(NICKNAME, nickname.getText().toString());
                            startActivity(myIntent);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast toastError = Toast.makeText(getApplicationContext(),"credenciales incorrectas", Toast.LENGTH_SHORT);
                        toastError.setGravity(Gravity.TOP, 0, 150);
                        toastError.show();


                        nickname.setText("");
                        password.setText("");


                        error.printStackTrace();
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<>();
                // the POST parameters:
                params.put("nick", usuario);
                params.put("pass", contrasena);
                return params;
            }
        };
        Volley.newRequestQueue(this).add(postRequest);

    }
}