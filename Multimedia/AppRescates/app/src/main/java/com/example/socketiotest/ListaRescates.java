package com.example.socketiotest;

import androidx.fragment.app.FragmentActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListaRescates extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String ip;
    private String rescates,usuario,nick;
    private JSONObject usu;
    private Marker myMarker;
    public GoogleMap.OnMarkerClickListener mListener;
   public JSONArray res = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_rescates);
        ip = getIntent().getStringExtra("ip");
        usuario = getIntent().getStringExtra("usuario");




        mListener = new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                /*StringRequest getUsuario = new StringRequest(Request.Method.GET, ip+":3000/usuario/getUsuario/"+nick,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                usuario = response;
                                try {
                                    usu = new JSONObject(usuario);
                                    //datos[3] = usu.getString("idGrupo");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error",error.toString());
                    }
                }){
                    @Override
                    public Priority getPriority() {
                        return Priority.IMMEDIATE;
                    }
                };
                Volley.newRequestQueue(getApplicationContext()).add(getUsuario);*/
                AlertDialog.Builder a = new AlertDialog.Builder(ListaRescates.this);

                    a.setMessage("¿Quieres unirte a la busqueda?\nDatos de busqueda:\n"+marker.getTitle())
                            .setTitle(R.string.dialog_title)
                            .setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Log.e("si", "si");
                                    try {
                                        usu = new JSONObject(usuario);
                                       // Log.e("nick",usuario);
                                        nick = usu.getString("docs");
                                        usu = new JSONObject(nick);
                                        nick = usu.getString("nick");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Log.e("nick",nick);
                                    StringRequest asignarUsu = new StringRequest(Request.Method.GET, ip+":3000/usuario/asignarGrupo/"+nick+"/"+marker.getSnippet(),
                                            new Response.Listener<String>() {
                                                @Override
                                                public void onResponse(String response) {
                                                   // rescates = response;
                                                    Intent myIntent = new Intent(ListaRescates.this, ChatBoxActivity.class);
                                                    myIntent.putExtra("usuario", usuario);
                                                    //Log.e("main",rescates);
                                                    //myIntent.putExtra("rescates", rescates);
                                                    try {
                                                        JSONObject idGrupo = new JSONObject(response);
                                                        Log.e("ListaRescates",idGrupo.toString());
                                                        myIntent.putExtra("idGrupo",idGrupo.getString("idGrupo"));
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    myIntent.putExtra("ip", ip);
                                                    myIntent.putExtra("usernickname", nick);
                                                    startActivity(myIntent);
                                                    finish();
                                                    //Log.e("ListaRescates",response);
                                                }
                                            }, new Response.ErrorListener() {
                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e("error",error.toString());
                                        }
                                    });
                                    Volley.newRequestQueue(getApplicationContext()).add(asignarUsu);
                                }
                            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.e("no", "no");
                        }
                    });
                    a.create();
                    a.show();
                    mMap.addCircle(new CircleOptions()
                            .center(new LatLng(marker.getPosition().latitude,marker.getPosition().longitude))
                            .strokeColor(Color.RED)
                            .strokeWidth(2)
                            .fillColor(Color.argb(100,106,255,0))
                            .radius(10000));


                return false;
            }
        };
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera


        CameraPosition googlePlex = CameraPosition.builder()
                .target(new LatLng(40.43022363450862,-3.7353515625000004))
                .zoom(5)
                .bearing(0)
                .tilt(0)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 3000, null);
        StringRequest getRescates = new StringRequest(Request.Method.GET, ip+":3000/rescate/getRescates",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        rescates = response;
                        mMap.setOnMarkerClickListener(mListener);

                        try {

                            res = new JSONArray(rescates);
                            //String test = res.get(0).toString();
                           // Log.e("rescate", test );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        for (int i = 0; i < res.length(); i++) {
                            imprimirMarkers(i);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
            }
        });
        Volley.newRequestQueue(this).add(getRescates);


    }
    public void imprimirMarkers(int i){
        String a = null, x = null, y = null, titulo = null, radio = null,idGrupo = null,descripcion= null;
        JSONObject rescate = null;
        try {
            a = res.get(i).toString();
            rescate = new JSONObject(a);
            x = rescate.getString("x");
            y = rescate.getString("y");
            titulo = rescate.getString(("nombre"));
            idGrupo = rescate.getString("idGrupo");
            descripcion = rescate.getString("descripcion");
            radio = rescate.getString("radio");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        MarkerOptions m = new MarkerOptions()
                .position(new LatLng(Double.parseDouble(x),Double.parseDouble(y)))
                .title(titulo+"\n"+descripcion+"").snippet(idGrupo);
        myMarker = mMap.addMarker(m);
    }
}
