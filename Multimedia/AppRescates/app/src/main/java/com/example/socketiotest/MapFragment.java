package com.example.socketiotest;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.socketiotest.R;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

public class MapFragment extends Fragment{

    public String rescate,tracks;
    public String ip;
    public String[] datos;
    public ViewModelCompartido model = ChatBoxActivity.model;
    private Marker myMarker;
    private GoogleMap mMap;
    private LatLng pos;
    public Socket socket = ChatBoxActivity.socket;
    private Location anterior = null,aux;
    JSONObject res = null,tra1=null;
    JSONArray tra = null,coor = null;
    JSONObject usu = null;
    public MapFragment() {
        // Required empty public constructor
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        model.getSelectedLocation().observe(getViewLifecycleOwner(), new Observer<Location>() {
            @Override
            public void onChanged(Location location) {

                if (anterior == null){

                    anterior = location;

                }
                aux = anterior;
                anterior = location;
                LatLng ant = new LatLng(aux.getLatitude(),aux.getLongitude());
                LatLng pos = new LatLng(location.getLatitude(),location.getLongitude());
                // mMap.addPolyline(new PolylineOptions().add(pos,ant).width(5));
                pintarTrack(pos,ant,true);
                String[] data = {ant.toString(),pos.toString()};
                String post = "{anterior:{x:"+ant.latitude+",y:"+ant.longitude+"},posterior:{x:"+pos.latitude+",y:"+pos.longitude+"},nick:"+datos[2]+",idGrupo:"+datos[3]+"}";
                try {
                    JSONObject coord = new JSONObject(post);
                    socket.emit("sendTrack", coord);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("anterior",ant.toString());
                Log.e("posterior",pos.toString());

            }
        });
        socket.on("actualizar",new Emitter.Listener() {

            @Override
            public void call(final Object... args) {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // String data = (String) args[0];
                            try {
                                JSONObject track = new JSONObject(args[0].toString());
                                String grupo = track.getString("idGrupo");
                                if (grupo.equalsIgnoreCase(datos[3])) {
                                    JSONObject anterior = new JSONObject(track.getString("anterior"));
                                    LatLng ant = new LatLng(Double.parseDouble(anterior.getString("x")), Double.parseDouble(anterior.getString("y")));
                                    JSONObject posterior = new JSONObject(track.getString("posterior"));
                                    LatLng pos = new LatLng(Double.parseDouble(posterior.getString("x")), Double.parseDouble(posterior.getString("y")));
                                    // Log.e("responsetrack",data);
                                    pintarTrack(pos, ant, false);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                            // Toast.makeText(getContext(), data, Toast.LENGTH_SHORT).show();

                        }
                    });
                }
            }
        });
        StringRequest getTracks = new StringRequest(Request.Method.GET, ip+":3000/track/getTracks/"+datos[3],
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        tracks = response;


                        try {

                            tra1 = new JSONObject(tracks);
                            tra = new JSONArray(tra1.getString("json"));
                            for (int i = 0; i < tra.length(); i++) {
                                JSONObject t = new JSONObject(tra.get(i).toString());
                                coor = new JSONArray(t.getString("coord"));
                                LatLng ant;
                                LatLng pos;
                                LatLng aux;
                                for (int j = 0; j <coor.length()-1 ; j++) {
                                    JSONObject a = new JSONObject(coor.get(j).toString());

                                        JSONObject p = new JSONObject(coor.get(j+1).toString());
                                         ant = new LatLng(Double.parseDouble(a.getString("x")),Double.parseDouble(a.getString("y")));
                                         aux = ant;
                                         pos = new LatLng(Double.parseDouble(p.getString("x")),Double.parseDouble(p.getString("y")));
                                         ant = pos;
                                         pintarTrack(aux,pos,false);


                                    Log.e("coordenadas",coor.get(j).toString());

                                   // LatLng pos = new LatLng(location.getLatitude(),location.getLongitude());
                                }
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
            }
        });
        Volley.newRequestQueue(getContext()).add(getTracks);

    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        datos = model.getDatos();//rescates,ip,nick,idGrupo
        try {

            socket = IO.socket(datos[1]+":4000");

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);




        ip = datos[1];
        //Log.e("MapFragment",datos[3]);
        StringRequest getRescate = new StringRequest(Request.Method.GET, ip+":3000/rescate/getRescate/"+datos[3],
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        rescate = response;


                        try {

                            res = new JSONObject(rescate);
                            //String test = res.get(0).toString();
                            centrarMapa(new LatLng(Double.parseDouble(res.getString("x")),Double.parseDouble(res.getString("y"))),res.getString("radio"));
                             Log.e("rescate", res.toString() );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
            }
        });
        Volley.newRequestQueue(getContext()).add(getRescate);


       /* try {
            res = new JSONArray(rescates);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);  //use SuppoprtMapFragment for using in fragment instead of activity  MapFragment = activity   SupportMapFragment = fragment

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mMap.clear(); //clear old markers




            }
        });



        return rootView;
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

public void pintarTrack(LatLng pos,LatLng ant,boolean me){
        if(mMap!=null) {
            if(me)
                mMap.addPolyline(new PolylineOptions().add(pos, ant).width(5).color(Color.RED));
            else
                mMap.addPolyline(new PolylineOptions().add(pos, ant).width(5).color(Color.BLUE));
        }
}
    public void setPos(LatLng pos) {
        this.pos = pos;
    }

    public void centrarMapa(LatLng pos,String d){

        CameraPosition googlePlex = CameraPosition.builder()
                .target(pos)
                .zoom((18-Float.parseFloat(d)/2))
                .bearing(0)
                .tilt(0)
                .build();

        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 3000, null);
        mMap.addMarker(new MarkerOptions().position(pos));
        mMap.addCircle(new CircleOptions().center(pos)
                .strokeColor(Color.RED)
                .strokeWidth(2)
                .fillColor(Color.argb(100,106,255,0))
                .radius(Double.parseDouble(d)*1000));
    }

}
