package com.example.socketiotest;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ChatBoxActivity extends AppCompatActivity {
    public RecyclerView myRecylerView;
    public List<Message> MessageList;
    public ChatBoxAdapter chatBoxAdapter;
    public EditText messagetxt;
    public Button send,salir;
    public String[] datos;
    public static Socket socket;
    public String usu,rescates;
    public String Nickname,nick;
    public JSONObject json,usuario;
    public JSONArray res;
    public String idUsuario,parseUsu,idGrupo;
    public LocationTrack locationTrack;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;

    public static ViewModelCompartido model = new ViewModelCompartido();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_box);

        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)

                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }

        locationTrack = new LocationTrack(ChatBoxActivity.this);


        if (locationTrack.canGetLocation()) {


            double longitude = locationTrack.getLongitude();
            double latitude = locationTrack.getLatitude();

           // Toast.makeText(getApplicationContext(), "Longitude:" + Double.toString(longitude) + "\nLatitude:" + Double.toString(latitude), Toast.LENGTH_SHORT).show();
        } else {

            locationTrack.showSettingsAlert();
        }
        addFragment(new MapFragment(), false, "one");
        rescates = getIntent().getStringExtra("rescates");


        usu = getIntent().getStringExtra("usuario");
        Log.e("Usuario",usu);

        try {


            json = new JSONObject(usu);
            parseUsu = json.getString("docs");

            usuario = new JSONObject(parseUsu);
            idUsuario = usuario.getString("_id");
            idGrupo = getIntent().getStringExtra("idGrupo");
            nick = usuario.getString("nick");

           // res = new JSONArray(rescates);
            //model.select(res);
           // JSONObject res1 = new JSONObject(res.get(0).toString());
            //Log.e("rescates", res1.getString("radio"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        datos = new String[]{rescates, getIntent().getStringExtra("ip"),nick,idGrupo};
        //Log.e("chatBoxActivity",rescates);
        model.setDatos(datos);
        messagetxt = (EditText) findViewById(R.id.message);
        send = (Button) findViewById(R.id.send);
        salir = findViewById(R.id.salirGrupo);

        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest  desAsignar  = new StringRequest(Request.Method.GET, getIntent().getStringExtra("ip")+":3000/usuario/desAsignarGrupo/"+datos[2],
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {

                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error",error.toString());
                    }
                });
                Volley.newRequestQueue(getApplicationContext()).add(desAsignar);
                Intent myIntent = new Intent(ChatBoxActivity.this, ListaRescates.class);
                myIntent.putExtra("usuario", usu);
                //Log.e("main",rescates);
                //myIntent.putExtra("rescates", rescates);
                myIntent.putExtra("ip", getIntent().getStringExtra("ip"));
                myIntent.putExtra("usernickname", nick);
                startActivity(myIntent);
                finish();
            }

        }
        );

        // get the nickame of the user
        Nickname = (String) getIntent().getExtras().getString(MainActivity.NICKNAME);
        //connect you socket client to the servertry {
        try {




            socket = IO.socket(getIntent().getStringExtra("ip")+":4000");

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        socket.connect();
        socket.emit("join", Nickname);

        //setting up recyler
        MessageList = new ArrayList<>();
        StringRequest  desAsignar  = new StringRequest(Request.Method.GET, getIntent().getStringExtra("ip")+":3000/mensaje/getMensajes/"+idGrupo,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                      //  Message m = new Message(nickname, response.,idGrupo);
                        try {
                            JSONObject json = new JSONObject(response);
                            JSONArray listaMensajes = new JSONArray(json.getString("json"));
                            for (int i = 0; i <listaMensajes.length() ; i++) {
                                JSONObject mensaje = new JSONObject(listaMensajes.get(i).toString());
                                //JSONObject men = new JSONObject(mensaje);
                                String nombre = mensaje.getString("nickUsuario")+": ";
                                Message m = new Message(nombre, mensaje.getString("mensaje"),idGrupo);
                                getMensajes(m);
                                chatBoxAdapter = new ChatBoxAdapter(MessageList);

                                // notify the adapter to update the recycler view

                                chatBoxAdapter.notifyDataSetChanged();

                                //set the adapter for the recycler view

                                myRecylerView.setAdapter(chatBoxAdapter);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.e("getMensajes",response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error",error.toString());
            }
        });
        Volley.newRequestQueue(getApplicationContext()).add(desAsignar);
        myRecylerView = (RecyclerView) findViewById(R.id.messagelist);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        myRecylerView.setLayoutManager(mLayoutManager);
        myRecylerView.setItemAnimator(new DefaultItemAnimator());


        // message send action
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //retrieve the nickname and the message content and fire the event messagedetection
                if(!messagetxt.getText().toString().isEmpty()) {
                    socket.emit("messagedetection", Nickname, messagetxt.getText().toString(), idUsuario,idGrupo);


                    messagetxt.setText("");
                }
            }


        });


        //implementing socket listeners
      /*  socket.on("userjoinedthechat", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String data = (String) args[0];

                        Toast.makeText(ChatBoxActivity.this, data, Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });
        socket.on("userdisconnect", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String data = (String) args[0];

                        Toast.makeText(ChatBoxActivity.this, data, Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });*/
        socket.on("message", new Emitter.Listener() {
            @Override
            public void call(final Object... args) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        JSONObject data = (JSONObject) args[0];
                        try {
                            //extract data from fired event

                            String nickname = data.getString("senderNickname");
                            String message = data.getString("message");
                            String idGrupoEx = data.getString("idGrupo");

                            // make instance of message
                            if(idGrupoEx.equalsIgnoreCase(idGrupo)){
                                Message m = new Message(nickname, message,idGrupo);


                                //add the message to the messageList

                                MessageList.add(m);

                                // add the new updated list to the dapter
                                chatBoxAdapter = new ChatBoxAdapter(MessageList);

                                // notify the adapter to update the recycler view

                                chatBoxAdapter.notifyDataSetChanged();

                                //set the adapter for the recycler view

                                myRecylerView.setAdapter(chatBoxAdapter);
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }
        });
    }
    public void getMensajes(Message m){
        MessageList.add(m);
    }
    public void addFragment(Fragment fragment, boolean addToBackStack, String tag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.container_frame_back, fragment, tag);
        ft.commitAllowingStateLoss();
    }
    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission((String) perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission((String) perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ChatBoxActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        locationTrack.stopListener();
        socket.disconnect();
    }
}