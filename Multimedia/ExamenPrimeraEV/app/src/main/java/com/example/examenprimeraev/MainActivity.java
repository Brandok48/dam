package com.example.examenprimeraev;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button b = findViewById(R.id.login);

        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText nick = findViewById(R.id.nick);
                EditText pass = findViewById(R.id.pass);
                String name = nick.getText().toString();
                String passw = pass.getText().toString();
                if(name.equals("ivan")&&passw.equals("12345678")){
                    Intent nuevoIntent = new Intent(MainActivity.this,JuegoNumeros.class);
                    String nombre = nick.getText().toString();
                    nuevoIntent.putExtra("nombre",nombre);
                    startActivity(nuevoIntent);
                }else if(name.equals("")||passw.equals("")){
                    Toast.makeText(MainActivity.this, "Alguno de los campos está vacío", Toast.LENGTH_SHORT).show();

                }
                else{
                    nick.setText(null);
                    pass.setText(null);
                    Toast.makeText(MainActivity.this, "Error de Credenciales\nVuelva a intentarlo", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
