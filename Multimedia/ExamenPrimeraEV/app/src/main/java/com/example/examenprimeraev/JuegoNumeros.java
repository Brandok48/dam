package com.example.examenprimeraev;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class JuegoNumeros extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego_numeros);
        TextView myText = findViewById(R.id.saludo);
        myText.setText("Hola "+getIntent().getStringExtra("nombre"));
        final TextView intentos = findViewById(R.id.intentos);
        final Button b = findViewById(R.id.probar);

        b.setOnClickListener(new View.OnClickListener() {
            int j;
            int i = (int) Math.floor(Math.random()*9+1);
            @Override
            public void onClick(View v) {


                EditText numero = findViewById(R.id.numero);
                if(numero.getText().toString().equals("")){
                    Toast.makeText(JuegoNumeros.this, "Por favor introduce un numero", Toast.LENGTH_SHORT).show();

                }

                else if(j <9&&Integer.parseInt(numero.getText().toString())!=i){
                    numero.setText("");
                    j = j+1;
                    String res = "NUMERO DE INTENTOS "+j;
                    Toast.makeText(JuegoNumeros.this, "Número incorrecto\nVuelva a intentarlo", Toast.LENGTH_SHORT).show();

                    intentos.setText(res);

                }
                else if(Integer.parseInt(numero.getText().toString())==i){
                    Toast.makeText(JuegoNumeros.this, "Enhorabuena! Has ganado!", Toast.LENGTH_SHORT).show();
                    numero.setEnabled(false);
                   // b.setOnClickListener(null);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(JuegoNumeros.this, "El juego ha terminado", Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                else{
                    numero.setText("");
                    j = j+1;
                    String res = "NUMERO DE INTENTOS "+j;
                    intentos.setText(res);
                    Toast.makeText(JuegoNumeros.this, "Has Perdido", Toast.LENGTH_SHORT).show();
                    numero.setEnabled(false);
                 //   b.setOnClickListener(null);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(JuegoNumeros.this, "El juego ha terminado", Toast.LENGTH_SHORT).show();

                        }
                    });
                }

            }
        });

    }
}
