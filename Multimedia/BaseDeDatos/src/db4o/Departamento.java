package db4o;

public class Departamento {
private String id;
private String nombre;
public Departamento(String id, String nombre) {
	super();
	this.id = id;
	this.nombre = nombre;
}
public Departamento() {
	super();
}
public String getId() {
	return id;
}
public void setId(String id) {
	this.id = id;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
}
