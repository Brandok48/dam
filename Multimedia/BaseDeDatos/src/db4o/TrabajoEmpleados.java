package db4o;

import java.util.Scanner;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;


public class TrabajoEmpleados {
	public static void generarBD() {
		ObjectContainer db = Db4o.openFile(BDPer); //Se crea la BBDD

		//Se crea el objeto Empleado

		Empleado p1 = new Empleado("f1","Ivan","Irun","d1");

		Empleado p2 = new Empleado ("f2","Cristian", "Zarautz","d1");

		Empleado p3 = new Empleado ("f3","Adrian", "Donostia","d2");

		//Se almacenan objetos Empleado en la Base de Datos

		db.store(p1);

		db.store(p2);

		db.store(p3);
		
		Departamento d1 = new Departamento("d1","Informatica");
		Departamento d2 = new Departamento("d2","Sistemas");
		Departamento d3 = new Departamento("d3","Produccion");
		//Se cierra la Base de datos
		
		db.store(d1);

		db.store(d2);

		db.store(d3);

		db.close();
	}
	final static String BDPer = "C:\\Users\\miket\\OneDrive\\Documentos\\_ivan\\dam\\Acceso a datos/EMPLEDEP.yap";
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int opcion = 1;
		while(opcion!=0) {
			try {
				System.out.println("-----------------------");
				System.out.println("| que quieres hacer:  |\n"
								 + "|  1. generar datos   |\n"
								 + "|  2. ver datos       |\n"
								 + "|  0. Terminar        |");
				System.out.println("-----------------------");
				opcion = sc.nextInt();
					switch (opcion) {
						case 1:
							generarBD();
							
							break;
						case 2:
							ObjectContainer db = Db4o.openFile(BDPer);

							Empleado per = new Empleado();
							Departamento dep = new Departamento(per.getFkDep(), null);
							ObjectSet result = db.queryByExample(per);
							

							if(result.size() == 0) System.out.println("No existen resgistros de personas en la base de datos");

							else {

							while(result.hasNext()) {

							Empleado p = (Empleado) result.next();

							System.out.println("Nombre: " + p.getNombre() + ", ciudad:" + p.getDireccion()+" Departamento:"+dep.getNombre());

							}

							}
							db.close();
							
							break;
						default:
							if(opcion<0||opcion>2)
							System.out.println("por favor introduzca un numero del 0 al 2");
				
			}
			}catch(Exception e) {
				System.out.println(e);
			}finally {
				sc.nextLine();
			}
			
	}
		sc.close();
		
		
		
	}
}
