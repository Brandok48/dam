package db4o;

import com.db4o.Db4o;
import com.db4o.ObjectContainer;

public class main {
	final static String BDPer = "C:\\Users\\2DAM3\\Documents\\dam\\Acceso a datos/DBPersonas.yap";
	public static void main(String[] args) {

		ObjectContainer db = Db4o.openFile(BDPer); //Se crea la BBDD

		//Se crea el objeto Persona

		Persona p1 = new Persona("Gorka","Irun");

		Persona p2 = new Persona ("Miren", "Ordizia");

		Persona p3 = new Persona ("Izaskun", "Donostia");

		//Se almacenan objetos Persona en la Base de Datos

		db.store(p1);

		db.store(p2);

		db.store(p3);

		//Se cierra la Base de datos
		

		db.close();
	}
}
