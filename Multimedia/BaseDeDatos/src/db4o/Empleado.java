package db4o;

public class Empleado {
	private String id;
	private String nombre;
	private String direccion;
	private String fkDep;
	
	
	public Empleado(String id,String nombre, String direccion,String fkdep) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		fkDep = fkdep;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Empleado() {
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getFkDep() {
		return fkDep;
	}
	public void setFkDep(String fkDep) {
		this.fkDep = fkDep;
	}
}
