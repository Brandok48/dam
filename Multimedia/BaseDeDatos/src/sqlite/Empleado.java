package sqlite;

public class Empleado {
	private int emp_no;
	private String apellido;
	private float salario;
	private int dept_no;
	public Empleado() {
		super();
	}
	public Empleado(int emp_no, String apellido, float salario, int dept_no) {
		super();
		this.emp_no = emp_no;
		this.apellido = apellido;
		this.salario = salario;
		this.dept_no = dept_no;
	}
	public int getEmp_no() {
		return emp_no;
	}
	public void setEmp_no(int emp_no) {
		this.emp_no = emp_no;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public float getSalario() {
		return salario;
	}
	public void setSalario(float salario) {
		this.salario = salario;
	}
	public int getDept_no() {
		return dept_no;
	}
	public void setDept_no(int dept_no) {
		this.dept_no = dept_no;
	}
}
