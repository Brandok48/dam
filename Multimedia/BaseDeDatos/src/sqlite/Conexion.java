package sqlite;
import java.sql.*;
import java.util.Scanner;
public class Conexion {
	static String url = "jdbc:sqlite:C:\\Users\\miket\\OneDrive\\Documentos\\_ivan\\sqlite-tools-win32-x86-3300000/DEPARTAMENTOS";
	static String url2 = "jdbc:mysql://localhost/ejemplo";
	public static Connection conectar() {
		Connection conn = null;
		
		try {
            // create a connection to the database
            conn = DriverManager.getConnection(url2,"root","root");
            
            System.out.println("Connection to SQLite has been established.");
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
		return conn;
	}
	
	public static void cerrarCon(Connection conn) {
		
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        
	}
	
	public static void consultarTabla() {
		String sql = "SELECT * FROM departamentos";
		Connection conn = null;
        
        try {
        	conn = conectar();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);
            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("dept_no") +  "\t" + 
                                   rs.getString("dnombre") + "\t" +
                                   rs.getString("loc"));
            }
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
        	cerrarCon(conn);
        }
        
	}
	public static void consultarMetadata() {
		Connection conn = null;
		try {
			conn = conectar();
			DatabaseMetaData metadata = conn.getMetaData();
			System.out.println("nombre del producto: "+metadata.getDatabaseProductName()+"\nVersion del producto: "+metadata.getDatabaseProductVersion());
		}catch(SQLException e) {
			
		}finally {
			cerrarCon(conn);
		}
	}
	public static void crearTablaEmpleados() {
		String sql = ""
				+ "Create table empleados("
				+ "emp_no tinyint(2) not null primary key,"
				+ "apellido varchar(10),"
				+ "	  salario float(6,2),"
				+ "	  dept_no tinyint(2) not null references departamentos(dept_no));";
		Connection conn = null;
        
        try {
        	conn = conectar();
            Statement stmt  = conn.createStatement();
            stmt.executeUpdate(sql);
            }
        catch (SQLException e){
            	System.out.println(e);
            }finally {
        cerrarCon(conn);
        }
	}
	public static void crearEmpleado(Empleado emp) {
		String sql = "insert into empleados values(20,\"branson\",1000.00,10);";
		String sql2 = "insert into empleados values(?,?,?,?);";
		
		Connection conn = null;
		
        
        try {
        	conn = conectar();
            //Statement stmt  = conn.createStatement();
            PreparedStatement pe = conn.prepareStatement(sql2);
            pe.setInt(1, emp.getEmp_no());
            pe.setString(2, emp.getApellido());
            pe.setFloat(3, emp.getSalario());
            pe.setInt(4, emp.getDept_no());
            pe.executeUpdate();
            //stmt.executeUpdate(sql);
            }
        catch (SQLException e){
            	System.out.println(e);
            }
        finally {
        	cerrarCon(conn);
        }
	}
	public static void main(String[] args) {
		//conectar();
		consultarTabla();
		consultarMetadata();
		//crearTablaEmpleados();
		//crearEmpleado();
		Empleado emp = new Empleado();
		Scanner sc = new Scanner(System.in);
		System.out.println("introduce el id del empleado");
		emp.setEmp_no(sc.nextInt());
		sc.nextLine();
		System.out.println("introduce el apellido del empleado");
		emp.setApellido(sc.nextLine());
		System.out.println("introduce el salario del empleado");
		emp.setSalario(sc.nextFloat());sc.nextLine();
		System.out.println("introduce el departamento del empleado");
		emp.setDept_no(sc.nextInt());
		sc.close();
		crearEmpleado(emp);
	}
}
