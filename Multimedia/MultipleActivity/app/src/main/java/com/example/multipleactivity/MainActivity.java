package com.example.multipleactivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ShareCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button b = (Button)findViewById(R.id.button);
        Button br = (Button)findViewById(R.id.button2);
        Button m = (Button)findViewById(R.id.musica);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevoIntent = new Intent(MainActivity.this,SecondActivity.class);
                EditText hola = findViewById(R.id.editText);
                String nombre = hola.getText().toString();
                nuevoIntent.putExtra("nombre",nombre);
                startActivity(nuevoIntent);
            }
        });
        br.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevoIntent = new Intent(MainActivity.this,BrujulaLayout.class);
                startActivity(nuevoIntent);
            }
        });
        m.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent nuevoIntent = new Intent(MainActivity.this,MusicaLayout.class);
                startActivity(nuevoIntent);
            }
        });

    }
}
