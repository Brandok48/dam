

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class Barbero extends Thread{
	private boolean durmiendo;
	private Semaphore sillon;

	private ArrayList<Cliente> lista = new ArrayList<Cliente>();
	public Barbero( Semaphore sillon) {
		super();
		this.durmiendo = false;
		this.sillon = sillon;
	}
	
	public void run() {
		while(true) 
		
			if(getLista().isEmpty()) {
				if(probarADormir()) {
					try {
						//System.out.println(cola.availablePermits());
						synchronized (sillon) {
							sillon.acquire();
						}
						
						
						this.setDurmiendo(true);
						Main.barberoPonX("B", "-", "Durmiendo");
						//System.out.println("se duerme el barbero");
						sleep(10000);
						//sleep((long) Math.random()*10000+1000);
						synchronized (sillon) {
							sillon.release();
						}
						Main.barberoPonX("-", "B", "Se despierta el barbero");
						//System.out.println("se despierta el barbero");
						setDurmiendo(false);
							for(Cliente b:getLista()) {
								synchronized (b) {
									b.notify();
									//System.out.println("se notifica que se despierta");
								}
								
							}
						
						
						
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}else
					Main.barberoPonX("-", "B", "no se durmio el barbero");
				//	System.out.println("se intento dormir pero no pudo");
			}
			
		
	}
	
	public boolean probarADormir() {
		Random random = new Random();
	    
		return random.nextBoolean();
	}
	public boolean isDurmiendo() {
		return durmiendo;
	}

	public void setDurmiendo(boolean durmiendo) {
		this.durmiendo = durmiendo;
	}

	public Semaphore getSillon() {
		return sillon;
	}

	public void setSillon(Semaphore sillon) {
		this.sillon = sillon;
	}

	
	public synchronized ArrayList<Cliente> getLista() {
		return lista;
	}

	public synchronized void setLista(ArrayList<Cliente> lista) {
		this.lista = lista;
	}
	public synchronized void anadirCliente(Cliente c) {
		this.lista.add(c);
	}
	public synchronized void irseCliente(Cliente c) {
		this.lista.remove(c);
	}

	
}
