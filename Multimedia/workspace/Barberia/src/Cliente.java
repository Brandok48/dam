import java.util.concurrent.Semaphore;

public class Cliente extends Thread{
	private int idc;
	private boolean cortado = false;
    private int pelos;
    private int paciencia;
    
    private Semaphore sillon;
    private Barbero barbero;
    
    
	public Cliente(int id,Semaphore sillon,Barbero barbero) {

		this.idc = id;
		this.sillon = sillon;
		this.pelos = (int) (Math.random()*2000+1000);
		this.paciencia = (int) (Math.random()*5000+2000);
		this.barbero = barbero;
		
	}

	public int getPelos() {
		return pelos;
	}
	public void setPelos(int pelos) {
		this.pelos = pelos;
	}
	public int getPaciencia() {
		return paciencia;
	}
	public void setPaciencia(int paciencia) {
		this.paciencia = paciencia;
	}
	public void run() {
		sentar();
	}
	
		public synchronized void sentar() {
			
			if(barbero.getLista().size()<=5) {
				
				//System.out.println("Sentado el cliente "+id);
				Main.asientoPonX(this);
				barbero.anadirCliente(this);
				try {
					
						if(barbero.isDurmiendo()) {
							iniciarPaciencia();
						}
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						
						e1.printStackTrace();
					}
				while(!cortado) {
					 {
							
						}
						try {
							
							sillon.acquire();
							Main.asientoQuitaX(this);
							Main.barberoPonX(""+idc, "B", "Cortando barba...");
							//System.out.println("cortando el pelo al cliente " + id);
							sleep(pelos);
							Main.barberoPonX("-", "B", "Termina de cortar la barba");
							
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						sillon.release();
						//System.out.println("se va el cliente "+ id);
						
						cortado = true;
						barbero.irseCliente(this);
						
					
				}
			}else {
				System.out.println("la barber�a esta llena, se va el cliente "+idc);
			}
		}
		public int getIdc() {
			return idc;
		}

		public void setIdc(int id) {
			this.idc = id;
		}

		public synchronized void iniciarPaciencia() throws InterruptedException {
				this.wait(this.getPaciencia());	
				if(barbero.isDurmiendo()) {
					System.out.println("El cliente " + this.getIdc() + " se ha cansado de esperar y se ha ido");
					barbero.irseCliente(this);
					Main.asientoQuitaX(this);
					Main.imprimir();

					//System.out.println("se cansa y se va el cliente "+id);
					cortado = true;
				}
			
		}
	
	public Semaphore getSillon() {
		return sillon;
	}
	public void setSillon(Semaphore sillon) {
		this.sillon = sillon;
	}
	public Barbero getBarbero() {
		return barbero;
	}
	public void setBarbero(Barbero barbero) {
		this.barbero = barbero;
	}
	public boolean isCortado() {
		return cortado;
	}
	public void setCortado(boolean cortado) {
		this.cortado = cortado;
	}

}