
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class Main extends Thread{
	public static String[] asientosEspera = new String[5];
	public static String[] asientoBarbero = new String[2];	
	public static Map<Cliente, Integer> map = new HashMap<Cliente, Integer>();
	public static String log;
	public static void main(String[] args) {
		
		for (int i = 0; i < asientosEspera.length; i++) {
			asientosEspera[i] = "-";
		}	
		asientoBarbero[0] = "-";
		asientoBarbero[1] = "B";

		
		Semaphore sillon = new Semaphore(1);
		
		int i = 0;
		Barbero bar = new Barbero( sillon);
		bar.start();
		
		for (int j = 0; j < 20; j++) {
			
			try {
				sleep(1000);
				//sleep((long)Math.random()*5000+2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			new Cliente(i,sillon,bar).start();
			i++;
			//System.out.println(cola.getQueueLength());
		}
		
	}
	public synchronized static void imprimir() {
		System.out.print(" | ");
		for(String asiento : asientosEspera) {
			System.out.print(asiento + " | ");
		}
		System.out.print(" ----------- ");
		
		System.out.print(" | ");
		for(String asientoBarbero : asientoBarbero) {
			System.out.print(asientoBarbero + " | ");
		}
		System.out.print(log);
		System.out.println();
		log="";

	}
	public synchronized static void asientoPonX(Cliente cliente) {
		int i = 0;
		while(!asientosEspera[i].equals("-")) { // Busca un asiento donde sentarse
			i++;
		}
		map.put(cliente, i);
		//System.out.println("--->"+map.get(cliente));
		asientosEspera[i] = ""+cliente.getIdc();
		log = "Se sienta un cliente";
		imprimir(); // Cada vez que el array se modifica se imprime todo
	}
	public synchronized static void asientoQuitaX(Cliente cliente) {
		//System.out.println(map.get(cliente));
		asientosEspera[map.get(cliente)] = "-"; // obtengo la posicion donde se ha sentado ese cliente y le vaco el asiento
		map.remove(cliente);
	}
	public synchronized static void barberoPonX(String asiento, String barbero, String estado) {
    	Main.asientoBarbero[0] = asiento;
    	Main.asientoBarbero[1] = barbero;
    	
    	log = estado;
		imprimir(); // Cada vez que el array se modifica se imprime todo
	}




}
