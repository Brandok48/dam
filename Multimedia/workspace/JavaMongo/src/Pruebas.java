
import java.util.Iterator;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

public class Pruebas {
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		MongoClient mongoClient = new MongoClient("localhost",27017);
		MongoDatabase db = mongoClient.getDatabase("prueba");
		System.out.println(db.getName());
		FindIterable findIterable = db.getCollection("Empleado").find();
		Iterator iter = findIterable.iterator();
		while (iter.hasNext()) {
			Document doc = (Document) iter.next();
			System.out.println(doc.toJson());
		}
		mongoClient.close();
	}
}
