package serversYSockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Cliente{
	
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[]args) {
		Socket s;
		BufferedReader br;
		try {
			s = new Socket();
			InetSocketAddress addr = new InetSocketAddress("localhost",4848);
			s.connect(addr);
			//InputStream is = s.getInputStream();
			//System.out.println(is);
			
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			//while((linea = br.readLine()) != null) {
				System.out.println("Cliente: "+br.readLine()+" cliente numero "+s.getRemoteSocketAddress());
			//}
			OutputStream os = s.getOutputStream();
			String mensaje="";
			PrintWriter pr = new PrintWriter(os);

			
			while (!mensaje.equalsIgnoreCase("END")) {
				System.out.println("Elige opci�n\n"
						+ "texto\n"
						+ "expresion\n"
						+ "end\n");
				mensaje = sc.nextLine();
				pr.println(mensaje);
				pr.flush();
				switch (mensaje) {
				case "texto":
					//server: escribe un texto
					mensaje = br.readLine();
					System.out.println(mensaje);
					mensaje = sc.nextLine();
					
					pr.println(mensaje);
					pr.flush();
					//server: has escrito: 
					System.out.println(br.readLine());
					break;
				case "expresion":
					//server: escribe una expresion
					mensaje = br.readLine();
					System.out.println(mensaje);
					mensaje = sc.nextLine();
					pr.println(mensaje);
					pr.flush();
					//server: resultado
					mensaje = br.readLine();
					System.out.println(mensaje+"\n");
					break;
				case "end":
					
					pr.println(mensaje);
					pr.flush();
					break;
				}
				
				

			}
			System.out.println("conexion terminada con el servidor");
			
			
			
			s.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

}
