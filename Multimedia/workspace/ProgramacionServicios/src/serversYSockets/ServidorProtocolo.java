package serversYSockets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import javax.script.ScriptEngineManager;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

public class ServidorProtocolo extends Thread {
	public static ServerSocket server;
	public static Socket client;
	public ServidorProtocolo(Socket s) {
		client = s;
	}
	
	public static void main(String[] args) {
		try {
			server = new ServerSocket(4848);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		while(true) {
			try {
				
				new ServidorProtocolo(server.accept()).start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public void run() {

			//crearHilos();
			
				try {
					
					System.out.println("alguien se ha conectado");
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
					bw.write("hola\n");
					bw.flush();
					OutputStream os = client.getOutputStream();
					PrintWriter pr = new PrintWriter(os);
					
					BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
					String linea = "";
					
					while(!linea.equalsIgnoreCase("END") ) {
						linea = br.readLine();
						
						switch (linea) {
						case "texto":
							System.out.println();
							
							linea = "escribe algo:";
							
							pr.println(linea);
							pr.flush();
							linea = br.readLine();
							System.out.println("Servidor: "+linea+"\n");
							
							pr.println("Has escrito: "+linea);
							pr.flush();
							break;
						case "expresion":
							linea = "escribe Una expresion:";
							
							pr.println(linea);
							pr.flush();
							linea = br.readLine();
							System.out.println("Expresion a evaluar: "+linea);
							Object exp = calcular(linea);
							if (exp!=null) {
								pr.println("Resultado: "+exp.toString());
								pr.flush();
							}
							else {
								pr.println("No has escrito una expresion valida");
								pr.flush();
							}
							
							break;
						case "end":
							
							break;
						}
						
						
						
					}
					System.out.println("Terminado");
					
				} catch (IOException e) {
					e.printStackTrace();
					
				}
			
			
		}
	public static Object calcular(String exp) {
		ScriptEngineManager mgr = new ScriptEngineManager();
	    ScriptEngine engine = mgr.getEngineByName("JavaScript");
	    try {
			System.out.println();
			return engine.eval(exp);
		} catch (ScriptException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	    return null;
	}
	
}
