package swing;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.*;
public class Main extends JFrame{

	public static void main(String[] args) {
			JFrame f = new JFrame("Loro");
			f.setSize(450, 150);
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			f.setVisible(true);
			JButton hola = new JButton("Di Hola");
			JButton adios = new JButton("Di Adios");
			Container cp = f.getContentPane();
			JTextField texto = new JTextField(20);
			cp.setLayout(new FlowLayout());
			hola.addActionListener(new EventoHola(texto));
			adios.addActionListener(new EventoAdios(texto));
			cp.add(hola);
			cp.add(adios);
			cp.add(texto);
	}
	
	
}
