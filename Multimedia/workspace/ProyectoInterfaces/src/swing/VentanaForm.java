package swing;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class VentanaForm extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2389233989710223700L;
	private String[] estadoCivil = {"Soltero/a","Casado/a","Viudo/a","Comprometido/a","Union libre o de hecho","separado","en Relacion"};
	private ArrayList<Persona> pers = new ArrayList<Persona>();
	private JTextField nombre = new JTextField(20);
	private JTextField apellido1 = new JTextField(20);
	private JTextField apellido2 = new JTextField(20);
	private JTextArea tLista = new JTextArea(10,50);
	private JRadioButton hombre = new JRadioButton("Hombre");
	private JRadioButton mujer = new JRadioButton("Mujer");
	private JComboBox<String> estado = new JComboBox<String>(estadoCivil);
	public VentanaForm() {
		super("Loro");
		ButtonGroup group = new ButtonGroup();
		group.add(hombre);
		group.add(mujer);
		setSize(600, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel p = new JPanel(new GridLayout(6,2));
		JPanel lista = new JPanel(new FlowLayout());
		JButton guardar = new JButton("Guardar");
		JButton cerrar = new JButton("Cerrar");
		Container cp = getContentPane();
		cerrar.addActionListener(new ActionListener() 
		{
		     public void actionPerformed(ActionEvent e)
		     {
		         System.exit(0);
		     }
		});
		cp.setLayout(new FlowLayout());
		guardar.addActionListener(new Guardar(nombre.getText(),tLista));
		//cp.add(hola);
		lista.setBorder(BorderFactory.createTitledBorder(
		        BorderFactory.createEtchedBorder(), "Listado"));
		p.setBorder(BorderFactory.createTitledBorder(
		        BorderFactory.createEtchedBorder(), "Introducir datos"));
		p.setSize(10,10);
		p.add(new JLabel("Nombre"));
		p.add(nombre);
		p.add(new JLabel("Apellido paterno"));
		p.add(apellido1);
		p.add(new JLabel("Apellido materno"));
		p.add(apellido2);
		p.add(new JLabel("Estado Civil"));
		p.add(estado);
		p.add(hombre);
		p.add(mujer);
		//p.add(guardar);
		lista.add(tLista);
		
		cp.add(p);
		cp.add(guardar);
		cp.add(cerrar);
		cp.add(lista);
	}
	
	
	
	class Guardar implements ActionListener{
		private JTextArea t;
		public Guardar(String s,JTextArea list) {
			t = list;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String p = "";
			if(hombre.isSelected()) {
				pers.add(new Persona(nombre.getText(),apellido1.getText(),apellido2.getText(),estado.getSelectedItem().toString(),"Hombre"));
			}else if(mujer.isSelected()) {
				pers.add(new Persona(nombre.getText(),apellido1.getText(),apellido2.getText(),estado.getSelectedItem().toString(),"Mujer"));
			}
			else {
				pers.add(new Persona(nombre.getText(),apellido1.getText(),apellido2.getText(),estado.getSelectedItem().toString(),"otro"));
			}
			
			nombre.setText("");
			apellido1.setText("");
			apellido2.setText("");
			for(int i = 0;i<pers.size();i++) {
			p += "Nombre:"+pers.get(i).getNombre().toString()+" \nApellido Paterno: "+pers.get(i).getApellido1().toString()+"\nApellido Materno: "+pers.get(i).getApellido2().toString()+"\nEstado Civil: "+pers.get(i).getEstadoCivil().toString()+"\nGenero: "+pers.get(i).getGenero().toString()+"\n\n";
			}
			t.setText(p);
		}
	
	}
}
