package swing;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.BoxLayout;
import javax.swing.WindowConstants;

public class Calculadora extends JFrame{
	JFrame frame;
	JPanel panelSuperior, panelInferior;
    JButton bt1, bt2, bt3, bt4, bt5, bt6, bt7, bt8, bt9, bt0, btRT, btCE, btCL, btMas, btMenos, btMul, btDiv, btIgual, btMN, btPunto,btMem;
    JTextField pantalla;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Calculadora(){
        construyePanelSuperior();
        construyePanelInferior();
        construyeVentana();}
	void construyePanelInferior(){
		panelInferior= new JPanel();
        panelInferior.setLayout(new GridLayout(4,6,8,8));
        bt0 = new JButton("0");
        bt1 = new JButton("1");
        bt2 = new JButton("2");
        bt3 = new JButton("3");
        bt4 = new JButton("4");
        bt5 = new JButton("5");
        bt6 = new JButton("6");
        bt7 = new JButton("7");
        bt8 = new JButton("8");
        bt9 = new JButton("9");
        btCE = new JButton("CE");
        btCL = new JButton("CL");
        btDiv = new JButton("/");
        btIgual = new JButton("=");
        btMas = new JButton("+");
        btMenos = new JButton("-");
        btMul = new JButton("*");
        panelInferior.add(bt7);
        panelInferior.add(bt8);
        panelInferior.add(bt9);
        panelInferior.add(btMul);
        panelInferior.add(btMas);
        panelInferior.add(bt4); 
        panelInferior.add(bt5);
        panelInferior.add(bt6);
        panelInferior.add(btDiv);
        panelInferior.add(btMenos);
        panelInferior.add(bt1);
        panelInferior.add(bt2);
        panelInferior.add(bt3);
        JPanel igual = new JPanel();
        igual.setLayout(new GridLayout());
        igual.add(btIgual);
        panelInferior.add(igual);
        panelInferior.add(bt0);
        
        panelInferior.add(btCE);
        panelInferior.add(btCL);
       
        
        
        
        
        
	}
    void construyePanelSuperior(){
        panelSuperior = new JPanel ();
        panelSuperior.setLayout(new FlowLayout());
        pantalla = new JTextField(20);
        panelSuperior.add(pantalla);
    }
	void construyeVentana(){
        frame =new JFrame("Calculadora ");
        frame.setLayout(new BoxLayout(frame.getContentPane(),BoxLayout.Y_AXIS));
        frame.add(panelSuperior);
        frame.add(panelInferior);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);}

	public static void main(String[] args) {
		new Calculadora();
		
	}
}
