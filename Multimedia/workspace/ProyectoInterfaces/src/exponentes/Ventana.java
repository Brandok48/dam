package exponentes;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;



public class Ventana extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5468077417439609795L;
	private JTextField base = new JTextField(20);
	private JTextField exponente = new JTextField(20);
	private JTextField result = new JTextField(20);
	public Ventana() {
		super("Exponentes");
		setSize(280, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel p = new JPanel(new GridLayout(6,2));
		JButton guardar = new JButton("Calcular");
		JButton cerrar = new JButton("Cerrar");
		JButton limpiar = new JButton("limpiar");
		Container cp = getContentPane();
		limpiar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				base.setText("");
				exponente.setText("");
				result.setText("");
				
			}
		});
		cerrar.addActionListener(new ActionListener() 
		{
		     public void actionPerformed(ActionEvent e)
		     {
		         System.exit(0);
		     }
		});
		guardar.addActionListener(new ActionListener() {
			
			@SuppressWarnings("static-access")
			@Override
			public void actionPerformed(ActionEvent e) {
				try{
				int bas = Integer.parseInt(base.getText());
				int exp = Integer.parseInt(exponente.getText());
				int aux = 1;
				
					while(exp>0) {
				
					aux*=bas;
					exp--;
					}
					result.setText(aux+"");
				
				}catch(Exception e2){
					JOptionPane panel = new JOptionPane("pepe");
					panel.showMessageDialog(null, "introduce un numero");
				}
				
				
			}
		});
		cp.setLayout(new FlowLayout());
		p.setSize(10,10);
		p.add(new JLabel("Base"));
		p.add(base);
		p.add(new JLabel("exponente"));
		p.add(exponente);
		p.add(new JLabel("resultado"));
		p.add(result);
		
		cp.add(p);
		
		cp.add(guardar);
		cp.add(limpiar);
		cp.add(cerrar);
		
	}
	

}
