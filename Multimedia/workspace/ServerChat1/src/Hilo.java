import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Hilo extends Thread{
	private int idc;
	Scanner sc = new Scanner(System.in);
	public Hilo(int idc) {
		super();
		this.idc = idc;
	}
	public void run() {
		Socket s;
		BufferedReader br;
		try {
			s = new Socket();
			InetSocketAddress addr = new InetSocketAddress("localhost",4848);
			s.connect(addr);
			//InputStream is = s.getInputStream();
			//System.out.println(is);
			
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			//while((linea = br.readLine()) != null) {
				System.out.println("Cliente: "+br.readLine()+" cliente numero "+idc);
			//}
			OutputStream os = s.getOutputStream();
			String mensaje="";

			

			while (!mensaje.equalsIgnoreCase("END")) {
				System.out.println("escribe algo:");
				mensaje = sc.next();
				String envio ="";
				
				envio = mensaje +"\n";
				os.write(envio.getBytes());
				os.flush();

			}
			
			
			
			s.close();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
	public int getIdc() {
		return idc;
	}
	public void setIdc(int idc) {
		this.idc = idc;
	}
}
