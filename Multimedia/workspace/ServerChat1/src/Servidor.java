

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor extends Thread {
	public static ServerSocket server;
	public static ArrayList<Socket> clientes = new ArrayList<Socket>();
	public static Socket client;
	public Servidor(Socket s) {
		client = s;
	}
	
	public static void main(String[] args) {
		try {
			server = new ServerSocket(4848);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	
		while(true) {
			try {
				client = server.accept();
				clientes.add(client);
				new Servidor(client).start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public void run() {

			//crearHilos();
			
				try {
					
					System.out.println("alguien se ha conectado");
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
					bw.write("hola\n");
					bw.flush();
					
					String linea = "";
					BufferedReader br = null;
					br = new BufferedReader(new InputStreamReader(client.getInputStream()));
					linea = br.readLine();
					while(!linea.equalsIgnoreCase("END") ) {
						
							
							
							broadcast(linea,client);
							
							System.out.println("Servidor: "+linea+"\n");
							linea = br.readLine();
							//pr.write(linea+"\n");
							//String s = linea +"\n";
							//bw.write(s);
							//bw.flush();
							if (linea == null) {
								clientes.remove(client);
							}
							
						
						
						}
					
						//linea = br.readLine();
						
						
					
					System.out.println("Terminado");
					
				} catch (IOException e) {
					e.printStackTrace();
					
				}
			
			
		}
	public static void broadcast(String linea,Socket c) {
		//System.out.println(clientes.size());
		for (Socket socket : clientes) {
			
			try {
			PrintWriter	pr = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
				pr.println(linea);
				pr.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
}
