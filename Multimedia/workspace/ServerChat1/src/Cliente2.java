import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Cliente2 {
	public static void main(String[] args) {
		iniciar();
	}
	public static void iniciar() {
		Socket s;
		BufferedReader br;
		try {
			s = new Socket();
			System.out.println("Creando la conexion");
			InetSocketAddress addr = new InetSocketAddress("localhost",4848);
			s.connect(addr);
			//InputStream is = s.getInputStream();
			//System.out.println(is);
			
			br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String linea = null;
			//while((linea = br.readLine()) != null) {
				System.out.println("Cliente: "+br.readLine());
			//}
			OutputStream os = s.getOutputStream();
			String mensaje = "Mensaje desde el cliente\nend";
			os.write(mensaje.getBytes());
			os.flush();
			System.out.println("mensaje enviado");
			System.out.println("cerrando el socket cliente");
			
			s.close();
			System.out.println("terminado");
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
}
