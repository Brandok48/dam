
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public static void main(String[] args) {

		try {
			ServerSocket ss = new ServerSocket(4848);

			while (true) {
				System.out.println("esperando...");
				Socket s = ss.accept();
				System.out.println("cliente conectado!");

				BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));

				String msg = null;
				while ((msg = br.readLine()) != null) {
					msg = "respuesta server:" + msg;
					bw.write(msg + "\n");
					bw.flush();
				}

				System.out.println("Cliente desconectado!");
			}
		} catch (IOException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}