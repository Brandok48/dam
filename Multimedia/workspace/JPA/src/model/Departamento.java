package model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import model.Empleado;
import java.util.Collection;
import javax.persistence.OneToMany;


/**
 * The persistent class for the departamentos database table.
 * 
 */
@Entity
@Table(name="departamentos")
@NamedQuery(name="Departamento.findAll", query="SELECT d FROM Departamento d")
public class Departamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="dept_no")
	private int deptNo;

	private String dnombre;

	private String loc;

	@OneToMany(mappedBy = "departamento")
	private Collection<Empleado> empleado;

	public Departamento() {
	}

	public int getDeptNo() {
		return this.deptNo;
	}

	public void setDeptNo(byte deptNo) {
		this.deptNo = deptNo;
	}

	public String getDnombre() {
		return this.dnombre;
	}

	public void setDnombre(String dnombre) {
		this.dnombre = dnombre;
	}

	public String getLoc() {
		return this.loc;
	}

	public void setLoc(String loc) {
		this.loc = loc;
	}

	public Collection<Empleado> getEmpleado() {
	    return empleado;
	}

	public void setEmpleado(Collection<Empleado> param) {
	    this.empleado = param;
	}

}