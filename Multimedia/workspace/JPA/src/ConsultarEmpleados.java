import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Empleado;

public class ConsultarEmpleados{
	static EntityManager entityManager;
	public static void main(String[] args) {
		//buscarEmpleado(69);
		//modificarEmpleado(12);
		insertarEmpleado();
		//eliminarEmpleado(69);
	}
	public static EntityManager crearManager() {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("JPA");
		entityManager = emfactory.createEntityManager();
		return entityManager;
	}
	public static void buscarEmpleado(int id) {
		
		entityManager = crearManager();
		model.Empleado empleado = entityManager.find(model.Empleado.class, id);
		
		if (empleado!=null) {
			//int i = empleado.getEmpNo();
			System.out.println("Empleado ID: "+empleado.getEmpNo());
			System.out.println("Empleado Apellido: "+empleado.getApellido());
			System.out.println("Empleado Salario: "+empleado.getSalario());
			
			model.Departamento dep = entityManager.find(model.Departamento.class, empleado.getDepartamento().getDeptNo());
			System.out.println("Empleado Departamento: "+dep.getDnombre());
		}
		
	}
	public static void modificarEmpleado(int id) {
		
		entityManager = crearManager();
		entityManager.getTransaction().begin();
		model.Empleado empleado = entityManager.find(model.Empleado.class, id);
		if (empleado!=null) {
			System.out.println("Resultado sin modificar");
			System.out.println("Empleado ID: "+empleado.getEmpNo());
			System.out.println("Empleado Apellido: "+empleado.getApellido());
			System.out.println("Empleado Salario: "+empleado.getSalario());
			
			model.Departamento dep = entityManager.find(model.Departamento.class, empleado.getDepartamento().getDeptNo());
			System.out.println("Empleado Departamento: "+dep.getDnombre());
		}
		empleado.setSalario(4848);
		entityManager.getTransaction().commit();
		if (empleado!=null) {
			System.out.println("resultado modificado");
			System.out.println("Empleado ID: "+empleado.getEmpNo());
			System.out.println("Empleado Apellido: "+empleado.getApellido());
			System.out.println("Empleado Salario: "+empleado.getSalario());
			
			model.Departamento dep = entityManager.find(model.Departamento.class, empleado.getDepartamento().getDeptNo());
			System.out.println("Empleado Departamento: "+dep.getDnombre());
		}
		
	}
	
	public static void insertarEmpleado() {
			EntityManager manager = crearManager();
			
			model.Empleado emple = new Empleado();
			if (emple!=null) {
				manager.getTransaction().begin();
				emple.setApellido("julian");
				//emple.setDeptNo(10);
				emple.setEmpNo(49);
				emple.setSalario(200);
				manager.persist(emple);
				manager.getTransaction().commit();
			}
	}
	public static void eliminarEmpleado(int id) {
		EntityManager manager = crearManager();
		
		model.Empleado empleado = manager.find(model.Empleado.class, id);
		if (empleado!=null) {
			manager.getTransaction().begin();
			
			manager.remove(empleado);
			
			manager.getTransaction().commit();
		}
	}
}
