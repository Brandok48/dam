import java.sql.Connection;
import java.util.Scanner;
public class ClasePrincipal {
	
	public static void main(String[]args) {
		
		Scanner sc = new Scanner(System.in);
		AccesoADatos ad = new AccesoADatos();
		
		
		
		 int opcion = 1;
			while(opcion!=0) {
				try {
					//menu principal
					System.out.println("+----------------------------------------------------+");
					System.out.println("| que quieres hacer?                                 |\n"
									 + "|  1. Insertar disco                                 |\n"
									 + "|  2. Insertar artista                               |\n"
									 + "|  3. Consultar artista y sus discos                 |\n"
									 + "|  4. Modificar artista                              |\n"
									 + "|  5. Eliminar artista                               |\n"
									 + "|  6. Crear base de datos                            |\n"
									 + "|  0. Terminar                                       |");
					System.out.println("+----------------------------------------------------+");
					opcion = sc.nextInt();
					
						switch (opcion) {
							case 1:
								sc.nextLine();
								ad.crearDisco(sc);
								break;
							case 2:
								sc.nextLine();
								ad.crearArtista(sc);
								System.out.println("Artista creado correctamente\nPulsa intro para continuar");
								break;
							case 3:
								sc.nextLine();
								System.out.println("introduce el nombre del artista que quieres visualizar: ");
								Artista art = ad.consultar(sc);
								while (art.getNombre()==null) {
									System.out.println("Ese artista no existe, introduce otro");
									art = ad.consultar(sc);
								}
								System.out.println("Nombre del artista: "+art.getNombre());
								System.out.println("Apellido del artista: "+art.getApellido());
								System.out.println("Localidad del artista: "+art.getLocalidad());
								System.out.println();
								for(Disco disc: art.getListaDiscos()) {
									System.out.println("Nombre del Disco: "+disc.getNombre());
									System.out.println("Publicacion del disco: "+disc.getFecha_publi());
									System.out.println("-----------------");
									
								}
								System.out.println("Pulsa intro para continuar");
								
								break;
							case 4:
								sc.nextLine();
								ad.modificar(sc);
								break;
							case 5:
								sc.nextLine();
								ad.eliminar(sc);
								break;
							case 6:
								ad.crearBD();
								break;
							case 0:
								break;
							default:
								//if(opcion<0||opcion>6)
								System.out.println("por favor introduzca un numero del 0 al 6");
					
				}
				}catch(Exception e) {
					System.out.println("hola "+e);
				}finally {
					
					sc.nextLine();
				}
			}
			
		sc.close();
	}
}
