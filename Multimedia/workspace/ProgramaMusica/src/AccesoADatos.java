import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class AccesoADatos {
	String url = "jdbc:sqlite:.\\lib\\musica.db";
	
	public Connection conectar() {
		Connection conn = null;
		
		try {
            // create a connection to the database
            conn = DriverManager.getConnection(url,"root","root");
            
            System.out.println("Conexi�n a SQLite establecida.");
            
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
		return conn;
	}
	public void crearBD() {
		//creamos un nuevo archivo para la base de datos
		File directorio=new File("lib");
		directorio.mkdir(); 
		Connection conn = conectar();
		System.out.println("Se ha creado la base de datos en la carpeta lib");       
        
		try {
			Statement stmt = conn.createStatement();
			conn.setAutoCommit(false);
			String sqlDropArtista = "DROP TABLE IF EXISTS artista";
			String sqlCreateArtista = "CREATE TABLE artista (" + 
					"    id        INTEGER      PRIMARY KEY" + 
					"                           UNIQUE" + 
					"                           NOT NULL," + 
					"    nombre    VARCHAR (20) UNIQUE," + 
					"    apellido  VARCHAR (20)," + 
					"    localidad VARCHAR (20)" + 
					");";			
			
			String sqlDropDisco = "DROP TABLE IF EXISTS disco";
			String sqlCreateDisco = "CREATE TABLE disco (" + 
					"    id          INTEGER     PRIMARY KEY" + 
					"                             UNIQUE" + 
					"                             NOT NULL," + 
					"    nombre      VARCHAR (50)," + 
					"    fecha_publi varchar(50)," + 
					"    id_artista  INTEGER (3)  REFERENCES artista (id)" + 
					"                             NOT NULL" + 
					");";

			stmt.executeUpdate(sqlDropArtista);
			stmt.executeUpdate(sqlCreateArtista);
			stmt.executeUpdate(sqlDropDisco);	
			stmt.executeUpdate(sqlCreateDisco);
			conn.commit();
		}catch(Exception e) {
			try {
				conn.rollback();
			} catch (SQLException e1) {
				
				e1.printStackTrace();
			}
			System.out.println("Error: " + e.getMessage());	
		}finally {
			try {
				conn.close();
			} catch (SQLException e) {
				
				e.printStackTrace();
			}	
		}
		
		


	}
	public void cerrarCon(Connection conn) {
		
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        
	}
	public void crearDisco(Scanner sc) {
		Disco disc = new Disco();
		String date = "";
		System.out.println("Introduce el nombre del disco");
		disc.setNombre(sc.nextLine());
		System.out.print("introduce la fecha\nA�o: ");
		date+= sc.nextLine()+"-";
		System.out.print("Mes: ");
		date+= sc.nextLine()+"-";
		System.out.print("Dia: ");
		date+= sc.nextLine();
		disc.setFecha_publi(date);
		//tiene que existir el artista
		System.out.println("Introduce el id del artista: ");
		disc.setId_artista(sc.nextInt());
		String sql = "insert into disco(nombre,fecha_publi,id_artista) values(?,?,?);";
		
		Connection conn = null;
		
        
        try {
        	conn = conectar();
        	conn.setAutoCommit(false);
            PreparedStatement pe = conn.prepareStatement(sql);
            //hacemos referencia a los interrogantes del string y los sustituimos con datos
            pe.setString(1, disc.getNombre());
            pe.setString(2, disc.getFecha_publi());
            pe.setInt(3, disc.getId_artista());
            pe.executeUpdate();
            conn.commit();
            }
        catch (SQLException e){
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
        }
        finally {
        	//siempre cerramos la conexion con la base de datos
        	cerrarCon(conn);
        }
	}
	public void crearArtista(Scanner sc) {
		Artista artista = new Artista();
		System.out.print("Introduce el nombre del artista: ");
		artista.setNombre(sc.nextLine());
		System.out.print("Introduce el apellido del artista: ");
		artista.setApellido(sc.nextLine());
		System.out.print("Introduce la localidad del artista: ");
		artista.setLocalidad(sc.nextLine());
		String sql = "insert into artista(nombre,apellido,localidad) values(?,?,?);";
		
		Connection conn = null;
		
		
        try {
        	conn = conectar();
        	conn.setAutoCommit(false);
            PreparedStatement pe = conn.prepareStatement(sql);
            //lo mismo que al crear un disco, referenciamos los interrogantes y le damos datos
            pe.setString(1, artista.getNombre());
            pe.setString(2, artista.getApellido());
            pe.setString(3, artista.getLocalidad());
            pe.executeUpdate();
            conn.commit();
            }
        catch (SQLException e){
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
            	System.out.println(e);
            }
        finally {
        	cerrarCon(conn);
        }
	}
	public Artista consultar(Scanner sc){
		String id = sc.nextLine();
		Connection conn = conectar();
		Artista art = new Artista();
		String sql = "select fecha_publi,disco.id as did,id_artista,disco.nombre as dnombre,artista.nombre as anombre,artista.id as aid,apellido,localidad from disco inner join artista on disco.id_artista = artista.id where artista.nombre like '"+id+"';";
		try {
			Statement stat = conn.createStatement();
			ResultSet result = null;
			result = stat.executeQuery(sql);
			
			while(result.next()) {
				Disco disc =  new Disco();
				//buscamos todos los discos
				disc.setFecha_publi((result.getString("fecha_publi")));
				disc.setId(result.getInt("did"));
				disc.setId_artista(result.getInt("id_artista"));
				disc.setNombre((result.getString("dnombre")));
				//para solo rellenar el artista una vez
				if(art.getNombre()!=""){
					art.setNombre(result.getString("anombre"));
					art.setId(result.getInt("aid"));
					art.setApellido(result.getString("apellido"));
					art.setLocalidad(result.getString("localidad"));
				}
				art.anadirDisco(disc);
				
			
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
        	cerrarCon(conn);
        }
		return art;

	}
	public void eliminar(Scanner sc) {
		System.out.println("introduce el id del artista que deseas borrar: ");
		int id = sc.nextInt();
		Connection conn = conectar();
		String sql = "delete from disco where id_artista = '"+id+"'";
		String sql2 = "delete from artista where id ='"+id+"';";

		try {
			Statement stat = conn.createStatement();
			stat.executeUpdate(sql);
			stat.executeUpdate(sql2);
			
			conn.commit();
		} catch (SQLException e) {
			System.out.println("Ese usuario no existe");
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
        	cerrarCon(conn);
        }
	}
	public void modificar(Scanner sc) {
		System.out.println("introduce el id del artista que deseas modificar: ");
		int id = sc.nextInt();
		Connection conn = conectar();
		
		String sql2 = "update artista set nombre = ?, apellido = ?, localidad = ? where id ='"+id+"';";
		sc.nextLine();
		try {
			PreparedStatement pe = conn.prepareStatement(sql2);
			conn.setAutoCommit(false);
			System.out.println("Introduce el nuevo nombre");
			pe.setString(1, sc.nextLine());
			System.out.println("Introduce el nuevo apellido");
            pe.setString(2, sc.nextLine());
            System.out.println("Introduce la nueva localidad");
            pe.setString(3, sc.nextLine());
            pe.executeUpdate();
			conn.commit();
		} catch (SQLException e) {
			System.out.println("Ese usuario no existe");
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			e.printStackTrace();
		}finally {
        	cerrarCon(conn);
        }
		System.out.println("artista modificado correctamente\npulsa intro para continuar");
	}
}
