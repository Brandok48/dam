package model;
//esta entidad se utiliza para poder contar el numero de personas en cada grupo
public class GrupoContacto {
	private int grupo;
	private long numCon;
	public GrupoContacto(int grupo, long numCon) {
		super();
		this.setGrupo(grupo);
		this.setNumCon(numCon);
	}
	public GrupoContacto() {
		super();
	}
	public long getNumCon() {
		return numCon;
	}
	public void setNumCon(long numCon) {
		this.numCon = numCon;
	}
	public int getGrupo() {
		return grupo;
	}
	public void setGrupo(int grupo) {
		this.grupo = grupo;
	}
}
