
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Contacto;
import model.Grupo;
import model.GrupoContacto;
import model.Telefono;

public class Main {
	static EntityManager entityManager;
	static EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("Agenda");

	public static EntityManager crearManager() {

		entityManager = emfactory.createEntityManager();
		return entityManager;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int opcion = 1;
		while (opcion != 0) {

			try {
				// menu principal
				String s = "+-------------------Menu Principal-------------------+";
				System.out.println(s);
				System.out.print("| que quieres hacer?                                 |\n");
				anadirCaracter(s, null, '-', false);
				System.out.println("|  1. A�adir                                         |\n"
						+ "|  2. Consultar                                      |\n"
						+ "|  3. Modificar datos de un contacto                 |\n"
						+ "|  4. Asociar un contacto a uno o varios grupos      |\n"
						+ "|  5. Eliminar                                       |\n"
						+ "|  0. Salir                                          |");
				anadirCaracter(s, null, '-', false);
				opcion = sc.nextInt();

				switch (opcion) {
				case 1:
					menuAnadir(sc);

					break;
				case 2:
					menuConsultar(sc);

					break;
				case 3:
					// modificar contacto
					sc.nextLine();
					entityManager = crearManager();
					buscarContactos(true, null);
					System.out.println("selecciona el contacto que deseas editar");
					int contacto = sc.nextInt();
					Contacto con = entityManager.find(Contacto.class, contacto);
					while (con == null) {
						System.out.println("ese Contacto no existe, elige otro");
						contacto = sc.nextInt();
						con = entityManager.find(Contacto.class, contacto);
					}
					System.out.println("Introduce el nuevo nombre para " + con.getNombre());
					entityManager.getTransaction().begin();
					sc.nextLine();
					con.setNombre(sc.nextLine());
					entityManager.getTransaction().commit();
					entityManager.close();

					break;
				case 4:
					// asociar contacto a uno o varios grupos
					sc.nextLine();
					entityManager = crearManager();
					buscarContactos(true, null);
					System.out.println("selecciona el contacto que deseas asignar a uno o mas grupos:");
					int idC = sc.nextInt();
					Contacto c = entityManager.find(Contacto.class, idC);
					while (c == null) {
						System.out.println("ese Contacto no existe, elige otro");
						idC = sc.nextInt();
						c = entityManager.find(Contacto.class, idC);
					}
					buscarGrupo(true, false);
					System.out.println("selecciona el grupo a asignar");
					int idG = sc.nextInt();
					Grupo g = entityManager.find(Grupo.class, idG);
					while (g == null) {
						System.out.println("ese grupo no existe, elige otro");
						idG = sc.nextInt();
						g = entityManager.find(Grupo.class, idG);
					}
					List<Grupo> grps = c.getGrupos();
					grps.add(g);
					int confirmar = 1;
					while (confirmar != 0) {
						System.out.println("Quieres asignar a mas grupos?0/1");
						confirmar = sc.nextInt();
						if (confirmar != 0) {
							System.out.println("selecciona el grupo a asignar");
							idG = sc.nextInt();
							g = entityManager.find(Grupo.class, idG);
							while (g == null) {
								System.out.println("ese grupo no existe, elige otro");
								idG = sc.nextInt();
								g = entityManager.find(Grupo.class, idG);
							}
							grps.add(g);
						}

					}
					sc.nextLine();
					entityManager.getTransaction().begin();
					c.setGrupos(grps);

					entityManager.getTransaction().commit();
					break;
				case 5:
					menuEliminar(sc);

					break;
				case 0:
					break;
				default:

					System.out.println("por favor introduzca un numero del 0 al 5");

				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}
		sc.close();
	}

	// en esta funcion llamamos a las funciones de a�adir
	public static void menuAnadir(Scanner sc) {

		int opcion = 1;
		while (opcion != 0) {

			try {
				// menu a�adir
				String s = "+-----------------------A�adir-----------------------+";
				System.out.println(s);
				System.out.println("| que quieres a�adir?                                |");
				anadirCaracter(s, null, '-', false);
				System.out.println("|  1. Contacto                                       |\n"
						+ "|  2. Telefono                                       |\n"
						+ "|  3. Grupo                                          |\n"
						+ "|  0. Volver                                         |");
				anadirCaracter(s, null, '-', false);
				opcion = sc.nextInt();

				switch (opcion) {
				case 1:
					// nuevo contacto

					entityManager = crearManager();
					buscarContactos(true, null);
					sc.nextLine();
					Contacto c = new Contacto();
					System.out.println("Introduce un nombre");
					String nombre = sc.nextLine();
					entityManager.getTransaction().begin();
					c.setNombre(nombre);
					entityManager.persist(c);

					entityManager.getTransaction().commit();
					entityManager.close();

					break;
				case 2:
					// nuevo numero de telefono
					sc.nextLine();
					entityManager = crearManager();
					buscarContactos(true, null);
					System.out.println("Introduce el id del contacto deseado");
					int id = sc.nextInt();

					Contacto ct = entityManager.find(Contacto.class, id);
					while (ct == null) {
						System.out.println("ese contacto no existe, prueba otra vez");
						id = sc.nextInt();
						ct = entityManager.find(Contacto.class, id);
					}
					buscarTelefonos(true, ct, true, "");

					Telefono t = new Telefono();
					System.out.println("Introduce un numero de telefono");
					String telefono = sc.nextLine();
					while (telefono.length() != 9) {

						telefono = sc.nextLine();
						if (telefono.length() != 9) {
							System.out.println("numero de telefono no valido");
						}
					}

					entityManager.getTransaction().begin();

					t.setNumero(telefono);
					//t.setContacto(ct);
					ct.addTelefono(t);
					//ct.setTelefonos(telefonos);
					entityManager.refresh(ct);
					entityManager.persist(t);
					

					entityManager.getTransaction().commit();

					entityManager.close();

					break;
				case 3:
					// nuevo grupo
					sc.nextLine();
					entityManager = crearManager();
					buscarGrupo(true, false);
					Grupo g = new Grupo();
					System.out.println("Introduce un nombre");
					nombre = sc.nextLine();
					entityManager.getTransaction().begin();
					g.setNombre(nombre);
					entityManager.persist(g);

					entityManager.getTransaction().commit();
					entityManager.close();

					break;

				case 0:
					break;
				default:

					System.out.println("por favor introduzca un numero del 0 al 3");

				}
			} catch (Exception e) {
				// System.out.println(e);
				e.printStackTrace();
			}
		}

	}

	// en esta funcion llamamos a las funciones de consultar
	public static void menuConsultar(Scanner sc) {
		int opcion = 1;
		while (opcion != 0) {

			try {
				// menu consultar
				String s = "+----------------------Consultar---------------------+";
				System.out.println(s);
				System.out.println("| que quieres consultar?                             |");
				anadirCaracter(s, null, '-', false);
				System.out.println("|  1. Los datos y los tel�fonos de un contacto       |\n"
						+ "|  2. Los contactos de un grupo con sus tel�fonos    |\n"
						+ "|  3. Los grupos junto con el n� de contactos        |\n"
						+ "|  0. Volver                                         |");
				anadirCaracter(s, null, '-', false);
				opcion = sc.nextInt();

				switch (opcion) {
				case 1:
					// datos de un contacto
					sc.nextLine();
					entityManager = crearManager();
					buscarContactos(true, null);
					int id = sc.nextInt();

					Contacto ct = entityManager.find(Contacto.class, id);
					while (ct == null) {
						System.out.println("ese contacto no existe, prueba otra vez");
						id = sc.nextInt();
						ct = entityManager.find(Contacto.class, id);
					}
					buscarTelefonos(true, ct, true, "");
					entityManager.close();
					break;
				case 2:
					// contactos de un grupo y sus telefonos
					sc.nextLine();
					entityManager = crearManager();
					buscarGrupo(true, false);
					System.out.println("Selecciona el grupo del que deseas ver los contactos");
					id = sc.nextInt();
					Grupo gr = entityManager.find(Grupo.class, id);
					buscarContactos(true, gr);
					entityManager.close();
					break;
				case 3:
					// los grupos y cuantos contactos
					sc.nextLine();
					entityManager = crearManager();

					// System.out.println(cantidad.getNumCon());
					buscarGrupo(true, true);
					entityManager.close();

					break;

				case 0:
					break;
				default:

					System.out.println("por favor introduzca un numero del 0 al 3");

				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}

	// en esta funcion llamamos a las funciones de eliminar
	public static void menuEliminar(Scanner sc) {
		int opcion = 1;
		while (opcion != 0) {

			try {
				// menu eliminar
				String s = "+----------------------Eliminar----------------------+";
				System.out.println(s);
				System.out.println("| que quieres eliminar?                              |");
				anadirCaracter(s, null, '-', false);
				System.out.println("|  1. Contacto                                       |\n"
						+ "|  2. Grupo                                          |\n"
						+ "|  0. Volver                                         |");
				anadirCaracter(s, null, '-', false);
				opcion = sc.nextInt();

				switch (opcion) {
				case 1:
					// eliminar contacto
					sc.nextLine();
					entityManager = crearManager();
					buscarContactos(true, null);
					System.out.println("Introduce el id del contacto a eliminar");
					int id = sc.nextInt();
					Contacto ct = entityManager.find(Contacto.class, id);
					while (ct == null) {
						System.out.println("ese contacto no existe, prueba otra vez");
						id = sc.nextInt();
						ct = entityManager.find(Contacto.class, id);
					}
					entityManager.getTransaction().begin();
					entityManager.remove(ct);
					entityManager.getTransaction().commit();
					entityManager.close();
					break;
				case 2:
					// eliminar grupo
					sc.nextLine();
					entityManager = crearManager();
					buscarGrupo(true, false);
					System.out.println("Introduce el id del contacto a eliminar");
					int idg = sc.nextInt();
					Grupo gr = entityManager.find(Grupo.class, idg);
					while (gr == null) {
						System.out.println("ese grupo no existe, prueba otra vez");
						id = sc.nextInt();
						gr = entityManager.find(Grupo.class, idg);
					}
					entityManager.getTransaction().begin();
					entityManager.remove(gr);
					entityManager.getTransaction().commit();
					entityManager.close();

					break;

				case 0:
					break;
				default:

					System.out.println("por favor introduzca un numero del 0 al 2");

				}
			} catch (Exception e) {
				System.out.println(e);
			}
		}

	}

	@SuppressWarnings("unchecked")
	public static List<Contacto> buscarContactos(boolean imprimir, Grupo gru) {
		// si imprimir es true se imprime la lista
		int i = 0;
		List<Contacto> con = entityManager.createNamedQuery("Contacto.findAll").getResultList();
		if (imprimir) {
			String s2;
			String s;
			if (gru == null) {
				s = "+--------Contactos----------+";
				System.out.println(s);
				i = 0;

				s2 = "| ID | Contacto";
				System.out.print(s2);

				anadirCaracter(s, s2, ' ', false);
				anadirCaracter(s, null, '-', true);
				for (Contacto contacto : con) {
					String s3 = contacto.getId() + "";
					i = s3.length();
					String s4 = "";

					if (i == 1)
						s4 += "  | ";
					if (i == 2)
						s4 += " | ";
					if (i == 3)
						s4 += "| ";

					i = 0;
					s2 = "| " + contacto.getId() + s4 + contacto.getNombre();
					System.out.print(s2);
					anadirCaracter(s, s2, ' ', false);

				}
				anadirCaracter(s, null, '-', true);
			} else {
				con = gru.getContactos();
				s = "+--------" + gru.getNombre() + "----------+";
				System.out.println(s);
				for (Contacto contacto : con) {
					s2 = "| ID | Contacto";
					System.out.print(s2);
					anadirCaracter(s, s2, ' ', false);
					String s3 = contacto.getId() + "";
					i = s3.length();
					String s4 = "";

					if (i == 1)
						s4 += "  | ";
					if (i == 2)
						s4 += " | ";
					if (i == 3)
						s4 += "| ";

					i = 0;
					s2 = "| " + contacto.getId() + s4 + contacto.getNombre();
					System.out.print(s2);
					anadirCaracter(s, s2, ' ', false);
					buscarTelefonos(true, contacto, false, s);
				}

			}

		}
		return con;
	}

	@SuppressWarnings("unchecked")
	public static List<Grupo> buscarGrupo(boolean imprimir, boolean contacto) {

		// si imprimir es true se imprime la lista
		int i = 0;
		List<Grupo> gru = entityManager.createNamedQuery("Grupo.findAll").getResultList();
		if (imprimir) {
			String s = "+--------Grupos----------+";
			System.out.println(s);
			i = 0;
			String s2;
			if (contacto) {
				s2 = "| ID | Grupo | Contactos";
			} else {
				s2 = "| ID | Grupo";
			}
			System.out.print(s2);

			anadirCaracter(s, s2, ' ', false);
			anadirCaracter(s, null, '-', true);
			for (Grupo grupo : gru) {
				int contactos = -1;
				if (contacto) {
					Query query = entityManager.createQuery(
							"SELECT NEW model.GrupoContacto (gru.id, COUNT(c)) FROM Contacto c JOIN c.grupos gru where gru.id=:numGr");
					query.setParameter("numGr", grupo.getId());
					GrupoContacto cantidad = (GrupoContacto) query.getSingleResult();
					contactos = (int) cantidad.getNumCon();
				}
				String s3 = grupo.getId() + "";
				i = s3.length();
				String s4 = "";

				if (i == 1)
					s4 += "  | ";
				if (i == 2)
					s4 += " | ";
				if (i == 3)
					s4 += "| ";

				i = 0;
				if (!contacto)
					s2 = "| " + grupo.getId() + s4 + grupo.getNombre();
				else
					s2 = "| " + grupo.getId() + s4 + grupo.getNombre() + " | " + contactos;
				System.out.print(s2);
				anadirCaracter(s, s2, ' ', false);
			}

			anadirCaracter(s, null, '-', true);

		}
		return gru;
	}

	public static List<Telefono> buscarTelefonos(boolean imprimir, Contacto con, boolean titulo, String su) {
		int i = 0;// esta variable es para poder formatear la impresion en el menu
		List<Telefono> tel = con.getTelefonos();
		if (imprimir) {
			String s;
			String s2;
			if (titulo) {
				s = "+--------Telefonos de " + con.getNombre() + "----------+";
				System.out.println(s);
				s2 = "| Id de " + con.getNombre() + ": " + con.getId();
				System.out.print(s2);

				// a�adimos los espacios necesarios
				anadirCaracter(s, s2, ' ', false);

			} else {

				s = su;
			}

			s2 = "| ID | Telefono";
			System.out.print(s2);
			anadirCaracter(s, s2, ' ', false);

			if (titulo)
				anadirCaracter(s, null, '-', true);

			for (Telefono t : tel) {

				String s3 = t.getId() + "";
				i = s3.length();
				String s4 = "";

				if (i == 1)
					s4 += "  | ";
				if (i == 2)
					s4 += " | ";
				if (i == 3)
					s4 += "| ";

				i = 0;
				s2 = "| " + t.getId() + s4 + t.getNumero();
				System.out.print(s2);
				// if(titulo)
				anadirCaracter(s, s2, ' ', false);

			}

			i = 0;
			anadirCaracter(s, null, '-', true);

		}
		return tel;
	}

	// funcion para formatear
	public static void anadirCaracter(String s, String s2, char c, boolean tabla) {
		int i = 0;
		if (s2 == null) {

			System.out.print("+");

			while (i < s.length() - 2) {
				if (i == 4 && tabla)
					System.out.print("+");
				else
					System.out.print(c);
				i++;
			}
			System.out.print("+\n");

		} else {
			while (i < (s.length() - s2.length() - 1)) {
				System.out.print(c);
				i++;
			}
			System.out.print("|\n");

		}

	}
}
