
public class Libro extends Ejemplar implements Imprimible{

	private String autor;
	private String editorial;
	
	
	public Libro(int id,String titulo,String autor, String editorial) {
		
		super(id,titulo);
		this.autor = autor;
		this.editorial = editorial;
	
	}
	public String getAutor() {
		return autor;
	}
	public void setAutor(String autor) {
		this.autor = autor;
	}
	public String getEditorial() {
		return editorial;
	}
	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}
	public void imprimir() {
		super.imprimir();
		System.out.println("Autor: "+autor);
		System.out.println("Editorial: "+editorial);
	}

	@Override
	public void reproducir() {
		System.out.println("Leyendo Libro.....");
	}
	@Override
	public void estampar() {
		// enviar a imprenta pepe
		
	}
}
