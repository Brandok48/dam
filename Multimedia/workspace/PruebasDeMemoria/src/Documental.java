
public class Documental extends Ejemplar{
	private String director;
	private String tematica;
	private String duracion;
	
	public Documental(int id,String titulo,String director,String tematica,String duracion) {
		super(id,titulo);
		this.director = director;
		this.tematica = tematica;
		this.duracion = duracion;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getTematica() {
		return tematica;
	}
	public void setTematica(String tematica) {
		this.tematica = tematica;
	}
	public String getDuracion() {
		return duracion;
	}
	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}
	public void imprimir() {
		super.imprimir();
		System.out.println("Director: "+director);
		System.out.println("Tem�tica: "+tematica);
		System.out.println("Ducaci�n: "+duracion);
		
	}
	@Override
	public void reproducir() {
		System.out.println("Reproduciendo....");
	}
}
