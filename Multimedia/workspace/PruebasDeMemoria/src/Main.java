import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		ArrayList<Ejemplar> lista = new ArrayList<Ejemplar>();
		for(int i = 0;i < 10;i++) {
			lista.add(new Libro(i,"Libro"+i,"Pepe"+i,"yaboi"));
		}
		
		lista.add(new Libro(988,"Libro","",""));
		lista.add(new Documental(988,"Documental","yeaaa","oh mama","1234"));
		System.out.println("----- datos del ejemplar");
		
		for(Ejemplar iej : lista) {
			iej.imprimir();
			System.out.println();
			//if(iej instanceof Libro)
				try {
					((Libro)iej).reproducir();
				}
				catch(Exception e) {
					System.out.println(e);
				}
				try {
					((Documental)iej).reproducir();
				}
				catch(Exception e) {
					System.out.println(e);
				}
			//((Libro)iej).leerLbro();
			/*else
				((Documental)iej).reproducir();*/
			
			System.out.println();
		}
		
		System.out.println("------------------------");
		Ejemplar ej = lista.get(6);
		
		System.out.println(((Libro) ej).getAutor());
	}
	
	
}
