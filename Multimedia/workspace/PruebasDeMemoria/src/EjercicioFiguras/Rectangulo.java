package EjercicioFiguras;
import java.util.Scanner;
public class Rectangulo extends FiguraGeometrica{
	
	
	private double base;
	private double altura;
	
	public Rectangulo(int x, int y,double base,double altura) {
		super(x, y);
		this.base = base;
		this.altura = altura;
	}
	public Rectangulo(Scanner sc) {
		super(0,0);
		boolean error = true;
		while(error){
			try {
				System.out.println("introduce la base: ");
				this.base = sc.nextDouble();
				System.out.println("introduce la altura: ");
				this.altura = sc.nextDouble();
				error = false;
			}catch(Exception e) {
				error = true;
				System.out.println("Por favor introduzca un numero\n");
				sc.next();
			}
		} 
	}
	public double getBase() {
		return base;
	}
	public void setBase(int base) {
		this.base = base;
	}
	public double getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	@Override
	public double calcularPerimetro() {
		super.setPerimetro(base*2+altura*2);
		return super.getPerimetro();
	}
	@Override
	public double calcularArea() {
		super.setArea(base*altura);
		return super.getArea();
	}
	/*@Override
	public void imprimir() {
		System.out.println("*****Rectángulo*****\n"
				 		 + "Area: "+super.getArea()+"\n"
				 		 + "Perimetro: "+super.getPerimetro()+"\n"
				 		 + "********************");
		
	}*/
	
}
