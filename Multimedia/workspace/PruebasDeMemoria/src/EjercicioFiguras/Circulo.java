package EjercicioFiguras;
import java.util.Scanner;
public class Circulo extends FiguraGeometrica{
	
	private double radio;
	private double diametro;
	
	public Circulo(int x,int y, double radio, double diametro) {
		super(x,y);
		this.radio = radio;
		this.diametro = diametro;
	}
	public Circulo(Scanner sc) {
		super(0,0);
		boolean error = true;
		while(error){
			try {
				
				System.out.println("introduce el radio: ");
				this.radio = sc.nextDouble();
				error = false;
			}catch(Exception e) {
				error = true;
				System.out.println("Por favor introduzca un numero\n");
				sc.next();
			}
		} 
	}
	public double getRadio() {
		return radio;
	}
	public void setRadio(int radio) {
		this.radio = radio;
	}
	public double getDiametro() {
		return diametro;
	}
	public void setDiametro(int diametro) {
		this.diametro = diametro;
	}
	@Override
	public double calcularPerimetro() {
		super.setPerimetro(2*radio*Math.PI);
		return super.getPerimetro();
	}
	@Override
	public double calcularArea() {
		super.setArea(Math.PI*Math.pow(this.radio,2));
		return super.getArea();
	}
	//@Override
	/*public void imprimir() {
		System.out.println("*****Circulo*****\n"
						 + "Area: "+calcularArea()+"\n"
						 + "Perimetro: "+calcularPerimetro()+"\n"
						 + "Radio: "+radio+"\n"
						 + "Diametro: "+diametro+"\n"
						 + "*****************");
		
	}*/
}
