package EjercicioFiguras;
import java.util.Scanner;
import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		//Triangulo tri = new Triangulo(0,0,10,9,9);
		Scanner sc = new Scanner(System.in);
		ArrayList<FiguraGeometrica> lista = new ArrayList<FiguraGeometrica>();
		
		/*System.out.println(tri.calcularArea());
		System.out.println("Equilatero: " + tri.esEquilatero());
		System.out.println("Escaleno: " + tri.esEscaleno());
		System.out.println("Isosceles: " + tri.esIsosceles());*/
		int opcion = 1;
		while(opcion!=0) {
			try {
				System.out.println("-----------------------");
				System.out.println("| que figura quieres: |\n"
								 + "|  1. Triangulo       |\n"
								 + "|  2. Circulo         |\n"
								 + "|  3. Rectangulo      |\n"
								 + "|  4. Calcular total  |\n"
								 + "|  0. Terminar        |");
				System.out.println("-----------------------");
				opcion = sc.nextInt();
					switch (opcion) {
						case 1:
							//pasamos un Scanner como parametro
							lista.add(new Triangulo(sc));
							break;
						case 2:
							lista.add(new Circulo(sc));
							break;
						case 3:
							lista.add(new Rectangulo(sc));
							break;
						case 4:
							double area1 = 0;
							double perimetro1 = 0;
							int j = 0;
							for(FiguraGeometrica iej : lista) {
								j++;
								area1 += iej.calcularArea();
								perimetro1 += iej.calcularPerimetro();
								iej.imprimir();
								//System.out.println("Perimetro total:"+iej.calcularPerimetro());
							}
							System.out.println("Area total: "+area1);
							System.out.println("Perimetro total: "+perimetro1);
							System.out.println("N�mero de figuras: "+j);
							break;
						case 0:
							double area = 0;
							double perimetro = 0;
							int i = 0;
							for(FiguraGeometrica iej : lista) {
								i++;
								area += iej.calcularArea();
								perimetro += iej.calcularPerimetro();
								iej.imprimir();
								//System.out.println("Perimetro total:"+iej.calcularPerimetro());
							}
							System.out.println("Area total: "+area);
							System.out.println("Perimetro total: "+perimetro);
							System.out.println("N�mero de figuras: "+i);
							System.out.println("\n***FIN DEL PROGRAMA***");
							break;
						default:
							if(opcion<0||opcion>4)
							System.out.println("por favor introduzca un numero del 0 al 4");
				
			}
			}catch(Exception e) {
				System.out.println("Por favor introduzca un numero del 0 al 4");
			}finally {
				sc.nextLine();
			}
		}
		
		sc.close();
	}
	
	
	
}
