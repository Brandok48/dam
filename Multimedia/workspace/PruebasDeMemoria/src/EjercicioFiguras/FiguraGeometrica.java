package EjercicioFiguras;

public abstract class FiguraGeometrica implements Imprimible{
	
	private double area;
	private double perimetro;
	private int x,y;
	
	
	public abstract double calcularPerimetro();
	public abstract double calcularArea();
	
	public FiguraGeometrica(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	public double getPerimetro() {
		return perimetro;
	}
	public void setPerimetro(double perimetro) {
		this.perimetro = perimetro;
	}
	public void imprimir() {
		String s = this.getClass().getName();
		String sub = s.substring(s.indexOf(".")+1);
		System.out.println("Figura: "+sub
						 + "\nArea: "+area+"\n"
						 + "Perimetro: "+perimetro+"\n");
	}
	
}
