
public abstract class Ejemplar {

	private int id;
	private String titulo;
	
	
	public Ejemplar() {
		
	}
	public Ejemplar(int id, String titulo) {
		this.id = id;
		this.titulo = titulo;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
	public void imprimir(){
		System.out.println("Id: "+this.id+
						  "\nTitulo: "+this.titulo);
		
	}
	public abstract void reproducir();
	
}
