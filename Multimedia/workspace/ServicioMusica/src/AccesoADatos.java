import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import clases.Artista;
import clases.Disco;

public class AccesoADatos {
	String url = "jdbc:mysql://localhost/musica";
		public Connection conectar() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			
            // create a connection to the database
			
            conn = DriverManager.getConnection(url,"root","root");
            
            System.out.println("Connection to SQLite has been established.");
            
        } catch (SQLException e) {
            e.getMessage();
            
        } 
		return conn;
	}
	
	public void cerrarCon(Connection conn) {
		
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        
	}
	public void crearDisco(Scanner sc){
		Disco disc = new Disco();
		String date = "";
		System.out.println("Introduce el nombre del disco");
		disc.setNombre(sc.nextLine());
		System.out.print("introduce la fecha\nA�o: ");
		date+= sc.nextLine()+"-";
		System.out.print("Mes: ");
		date+= sc.nextLine()+"-";
		System.out.print("Dia: ");
		date+= sc.nextLine();
		disc.setFecha_publi(Date.valueOf(date));
		System.out.println("Introduce el id del artista: ");
		disc.setId_artista(sc.nextInt());
		String sql = "insert into disco(nombre,fecha_publi,id_artista) values(?,?,?);";
		
		Connection conn = null;
		
        
        try {
        	conn = conectar();
        	conn.setAutoCommit(false);
            PreparedStatement pe = conn.prepareStatement(sql);
            pe.setString(1, disc.getNombre());
            pe.setDate(2, disc.getFecha_publi());
            pe.setInt(3, disc.getId_artista());
            pe.executeUpdate();
            conn.commit();
            }
        catch (SQLException e){
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
            	System.out.println(e);
            }
        finally {
        	cerrarCon(conn);
        }
	}
	public void crearArtista(Scanner sc){
		Artista artista = new Artista();
		System.out.print("Introduce el nombre del artista: ");
		artista.setNombre(sc.nextLine());
		System.out.print("Introduce el apellido del artista: ");
		artista.setApellido(sc.nextLine());
		System.out.print("Introduce la localidad del artista: ");
		artista.setLocalidad(sc.nextLine());
		String sql = "insert into artista(nombre,apellido,localidad) values(?,?,?);";
		
		Connection conn = null;
		
		
        try {
        	conn = conectar();
        	conn.setAutoCommit(false);
            PreparedStatement pe = conn.prepareStatement(sql);
            pe.setString(1, artista.getNombre());
            pe.setString(2, artista.getApellido());
            pe.setString(3, artista.getLocalidad());
            pe.executeUpdate();
            conn.commit();
            }
        catch (SQLException e){
        	try {
				conn.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
            	System.out.println(e);
            }
        finally {
        	cerrarCon(conn);
        }
	}
	public Artista consultar(String nombre){
		Connection conn = conectar();
		Artista art = new Artista();
		String sql = "select * from disco inner join artista on disco.id_artista = artista.id where artista.nombre like '"+nombre+"';";
		try {
			Statement stat = conn.createStatement();
			ResultSet result = null;
			result = stat.executeQuery(sql);
			
			while(result.next()) {
				Disco disc =  new Disco();
				disc.setFecha_publi((result.getDate("disco.fecha_publi")));
				disc.setId(result.getInt("disco.id"));
				disc.setId_artista(result.getInt("disco.id_artista"));
				disc.setNombre((result.getString("disco.nombre")));;
				if(art.getNombre()!=""){
					art.setNombre(result.getString("artista.nombre"));
					art.setId(result.getInt("artista.id"));
					art.setApellido(result.getString("artista.apellido"));
					art.setLocalidad(result.getString("artista.localidad"));
				}
				art.anadirDisco(disc);
				
			//System.out.println("Id: "+art.getId()+"\nNombre: "+art.getNombre()+"\nApellido: "+art.getApellido()+"\nLocalidad: "+art.getLocalidad()+"\nDisco: "+disc.getNombre()+"\nFecha de publicacion: "+disc.getFecha_publi()+"\n");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return art;

	}
}
