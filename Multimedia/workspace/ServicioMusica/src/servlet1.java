

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import clases.Artista;

/**
 * Servlet implementation class servlet1
 */
@WebServlet("/servlet1")
public class servlet1 extends HttpServlet {
	AccesoADatos ad = new AccesoADatos();
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public servlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		/*Artista art = new Artista();
		request.setAttribute("artista", art);*/
		request.setAttribute("error", "");
		request.getRequestDispatcher("index.jsp").forward(request,response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet(request, response);
		Artista art = ad.consultar(request.getParameter("id"));
		if(art.getNombre()!=null) {
		request.setAttribute("artista", art);
		request.getRequestDispatcher("datos.jsp").forward(request,response);
		
		}else {
			request.setAttribute("error", "No existe el artista");
			request.setAttribute("artista", art);
			request.getRequestDispatcher("index.jsp").forward(request,response);
		}
	}

}
