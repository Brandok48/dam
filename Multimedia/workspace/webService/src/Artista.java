import java.util.ArrayList;

public class Artista {
	private int id;
	private String nombre;
	private String apellido;
	private String localidad;
	private ArrayList<Disco> listaDiscos;
	public Artista() {
		
	}
	public Artista(String nombre, String apellido, String localidad) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.localidad = localidad;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getLocalidad() {
		return localidad;
	}
	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	public ArrayList<Disco> getListaDiscos() {
		return listaDiscos;
	}
	public void setListaDiscos(ArrayList<Disco> listaDiscos) {
		this.listaDiscos = listaDiscos;
	}
	public void anadirDisco(Disco disc) {
		listaDiscos.add(disc);
	}
	
	
	
}
