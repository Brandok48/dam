import java.sql.Connection;
import java.util.Scanner;
public class ClasePrincipal {
	
	public static void main(String[]args) {
		
		Scanner sc = new Scanner(System.in);
		AccesoADatos ad = new AccesoADatos();
		Connection conn = ad.conectar();
		
		
		 int opcion = 1;
			while(opcion!=0) {
				try {
					System.out.println("+----------------------------------------------------+");
					System.out.println("| que quieres hacer?                                 |\n"
									 + "|  1. Insertar disco                                 |\n"
									 + "|  2. Insertar artista                               |\n"
									 + "|  3. Consultar artista y sus discos                 |\n"
									 + "|  4. Modificar artista                              |\n"
									 + "|  5. Eliminar artista                               |\n"
									 + "|  0. Terminar                                       |");
					System.out.println("+----------------------------------------------------+");
					opcion = sc.nextInt();
						switch (opcion) {
							case 1:
								sc.nextLine();
								ad.crearDisco(sc);
								break;
							case 2:
								sc.nextLine();
								ad.crearArtista(sc);
								break;
							case 3:
								ad.consultar(sc.nextLine());
								break;
							case 4:
								
								
								break;
							case 5:
								
								break;
							case 0:
								
								break;
							default:
								if(opcion<0||opcion>5)
								System.out.println("por favor introduzca un numero del 0 al 5");
					
				}
				}catch(Exception e) {
					System.out.println("Por favor introduzca un numero del 0 al 5 "+e);
				}finally {
					ad.cerrarCon(conn);
					sc.nextLine();
				}
			}
			
		sc.close();
	}
}
