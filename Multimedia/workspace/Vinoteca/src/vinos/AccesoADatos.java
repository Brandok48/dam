package vinos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class AccesoADatos {
	String url = "jdbc:mysql://localhost/vinoteca";
	public Connection conectar() {
	Connection conn = null;
	try {
		Class.forName("com.mysql.jdbc.Driver");
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	}
	try {
		
        // create a connection to the database
		
        conn = DriverManager.getConnection(url,"root","root");
        
        System.out.println("Connection to SQLite has been established.");
        
    } catch (SQLException e) {
        e.getMessage();
        
    } 
	return conn;
}

public void cerrarCon(Connection conn) {
	
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    
}
}
