package vinos;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JButton;

public class Total {

	private JFrame frame;
	private DefaultTableModel mod;
	private ArrayList<Producto> table;
	private JTable tabla;
	private JButton btnComprar;
	private AccesoADatos ad = new AccesoADatos();
	int usuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Total window = new Total();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Total() {
		initialize();
	}
	public Total(ArrayList<Producto> table,int usuario) {
		this.table = table;
		this.usuario = usuario;
		initialize();
		
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		String[] columnas = {"cantidad","nombre","precio"};
		mod = new DefaultTableModel(null,columnas);
		tabla = new JTable();
		tabla.setEnabled(false);
		tabla.setModel(mod);
		mod.setColumnIdentifiers(columnas);
		//frame.getContentPane().add(this.table);
		mod.addRow(columnas);
		for (Producto p : table) {
			String pr[] = {p.getCantidad()+"",p.getNombre(),p.getPrecio()*p.getCantidad()+""};
			mod.addRow(pr);
		}
		
		JPanel panel = new JPanel();
		
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.add(tabla);
		
		btnComprar = new JButton("Comprar");
		btnComprar.addActionListener(new ActionListener() {
			ResultSet result;
			@Override
			public void actionPerformed(ActionEvent e) {
				int idCarro = 0;
				String sql = "insert into carro(idProd,cantidad) values(?,?);";
					Carro c = new Carro();
					Connection conn = null;
					conn = ad.conectar();
				for (Producto pro : table) {
					
			        try {
			        	
			        	conn.setAutoCommit(false);
			            PreparedStatement pe = conn.prepareStatement(sql);
			            pe.setInt(1, pro.getId());
			            c.setIdP(pro.getId());
			            pe.setInt(2, pro.getCantidad());
			            c.setCantidad(pro.getCantidad());
			            pe.executeUpdate();
			            conn.commit();
			            
			            }
			        catch (SQLException e3){
			        	try {
							conn.rollback();
						} catch (SQLException e1) {
							e1.printStackTrace();
						}
			            	System.out.println(e);
			            }
			        
				}
				sql = "select id from carro order by id desc;";
				Statement stat;
					try {
						stat = conn.createStatement();
						result = null;
						result = stat.executeQuery(sql);
					} catch (SQLException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					
					
				for(int i = 0; i< table.size();i++) {
					try {
					
					result.next();
					idCarro= result.getInt("id");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					sql = "insert into factura(idUsuario,idCarro)values("+usuario+","+idCarro+")";
					try {
						stat = conn.createStatement();
						conn.setAutoCommit(true);
						stat.execute(sql);
						
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
		        	ad.cerrarCon(conn);
		        JOptionPane.showMessageDialog(null, "Gracias por su compra");
		        System.exit(0);
			}
		});
		frame.getContentPane().add(btnComprar, BorderLayout.SOUTH);
		frame.setVisible(true);
	}

	public ArrayList<Producto> getTable() {
		return table;
	}

	public void setTable(ArrayList<Producto> table) {
		this.table = table;
	}

	

}
