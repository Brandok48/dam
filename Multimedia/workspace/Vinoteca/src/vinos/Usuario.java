package vinos;

public class Usuario {
	private int id;
	private String nombre;
	private String nombreusu;
	private String apellidos;
	private String contrasena;
	
	public Usuario() {
		
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getNombreusu() {
		return nombreusu;
	}
	public void setNombreusu(String nombreusu) {
		this.nombreusu = nombreusu;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public Usuario(int id,String nombre, String nombreusu, String apellidos, String contrasena) {
		super();
		this.id = id; 
		this.nombre = nombre;
		this.nombreusu = nombreusu;
		this.apellidos = apellidos;
		this.contrasena = contrasena;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}
