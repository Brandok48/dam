package vinos;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JCalendar;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Ventana extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AccesoADatos ad = new AccesoADatos();
	private ArrayList<Producto> factura = new ArrayList<Producto>();
	private JCalendar calendario = new JCalendar();
	private JFrame frmVinoteca;
	private JTable table,tabla;
	private DefaultTableModel mod,mod2;
	private Usuario usu;
	public static ArrayList<Producto> lista = new ArrayList<Producto>();

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					
					window.frmVinoteca.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ventana() {
		initialize();
	}
	public Ventana(Usuario usu) {
		initialize();
		this.usu=usu;
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		Connection conn = null;
		String sql = "select * from producto;";
		
		try {
			conn = ad.conectar();
			Statement stat = conn.createStatement();
			ResultSet result = null;
			result = stat.executeQuery(sql);
			conn.setAutoCommit(false);
			
			while(result.next()) {
				Producto p = new Producto();
				p.setId(result.getInt("producto.id"));
				p.setNombre(result.getString("producto.nombre"));
				p.setTipo(result.getString("producto.tipo"));
				p.setStock(result.getInt("producto.stock"));
				p.setPrecio(result.getDouble("producto.precio"));
				p.setStockMin(result.getInt("producto.stockmin"));
				p.setImagen(result.getString("producto.imagen"));
				lista.add(p);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			ad.cerrarCon(conn);
		}
		String[] columnas = {"cantidad","nombre","precio"};
		mod = new DefaultTableModel(null,columnas);
		String[] columnas2 = {"Articulo","stock","precio","stockmin"};
		mod2 = new DefaultTableModel(null,columnas2);
		frmVinoteca = new JFrame();
		frmVinoteca.setTitle("Vinoteca");
		frmVinoteca.setBounds(100, 100, 770, 500);
		frmVinoteca.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmVinoteca.getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		ImageIcon imagen1=new ImageIcon("imagenes/coca-cola-classic.jpg");
		Image imagen = imagen1.getImage();
		imagen = imagen.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		imagen1.setImage(imagen);
		
		
		JPanel Vino = new JPanel();
		tabbedPane.addTab("Vino", imagen1, Vino, null);
		Vino.setLayout(new GridLayout(0, 2, 0, 0));
		
		imagen1=new ImageIcon("imagenes/coca-cola-classic.jpg");
		imagen = imagen1.getImage();
		imagen = imagen.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		imagen1.setImage(imagen);
		
		JPanel Tabaco = new JPanel();
		tabbedPane.addTab("Tabaco", imagen1, Tabaco, null);
		Tabaco.setLayout(new GridLayout(0, 2, 0, 0));
		
		imagen1=new ImageIcon("imagenes/coca-cola-classic.jpg");
		imagen = imagen1.getImage();
		imagen = imagen.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		imagen1.setImage(imagen);
		
		JPanel Cafe = new JPanel();
		tabbedPane.addTab("Caf�", imagen1, Cafe, null);
		Cafe.setLayout(new GridLayout(0, 2, 0, 0));
		
		imagen1=new ImageIcon("imagenes/coca-cola-classic.jpg");
		imagen = imagen1.getImage();
		imagen = imagen.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		imagen1.setImage(imagen);
		JPanel Refrescos = new JPanel();
		tabbedPane.addTab("Refrescos", imagen1, Refrescos, null);
		Refrescos.setLayout(new GridLayout(0, 2, 0, 0));
		
		imagen1=new ImageIcon("imagenes/coca-cola-classic.jpg");
		imagen = imagen1.getImage();
		imagen = imagen.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		imagen1.setImage(imagen);
		JPanel Licores = new JPanel();
		tabbedPane.addTab("Licores", imagen1, Licores, null);
		Licores.setLayout(new GridLayout(0, 2, 0, 0));
		
		imagen1=new ImageIcon("imagenes/coca-cola-classic.jpg");
		imagen = imagen1.getImage();
		imagen = imagen.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		imagen1.setImage(imagen);
		JPanel Conservas = new JPanel();
		tabbedPane.addTab("Conservas", imagen1, Conservas, null);
		Conservas.setLayout(new GridLayout(0, 2, 0, 0));
		
		imagen1=new ImageIcon("imagenes/coca-cola-classic.jpg");
		imagen = imagen1.getImage();
		imagen = imagen.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		imagen1.setImage(imagen);
		JPanel Cocina = new JPanel();
		tabbedPane.addTab("Cocina", imagen1, Cocina, null);
		Cocina.setLayout(new GridLayout(0, 2, 0, 0));
		
		imagen1=new ImageIcon("imagenes/coca-cola-classic.jpg");
		imagen = imagen1.getImage();
		imagen = imagen.getScaledInstance(30,30, Image.SCALE_SMOOTH);
		imagen1.setImage(imagen);
		JPanel Helados = new JPanel();
		tabbedPane.addTab("Helados", imagen1, Helados, null);
		Helados.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel Reservas = new JPanel();
		tabbedPane.addTab("New tab", null, Reservas, null);
		Reservas.setLayout(new BorderLayout(0, 0));
		Reservas.add(calendario, BorderLayout.NORTH);
		
		JButton btnReservar = new JButton("Reservar");
		Reservas.add(btnReservar, BorderLayout.SOUTH);
		btnReservar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(calendario.getDate());
				
			}
		});
		
		
		JPanel panel_9 = new JPanel();
		frmVinoteca.getContentPane().add(panel_9, BorderLayout.EAST);
		
		table = new JTable();
		tabla = new JTable();
		
		table.setEnabled(false);
		tabla.setEnabled(false);
		
		table.setModel(mod);
		tabla.setModel(mod2);
		
		mod.setColumnIdentifiers(columnas);
		mod2.setColumnIdentifiers(columnas2);
		mod2.addRow(columnas2);
		//{"nombre","stock","precio","stockmin"};
		for (Producto pro : lista) {
			String[] p = {pro.getNombre(),pro.getStock()+"",pro.getPrecio()+"",pro.getStockMin()+""};
			mod2.addRow(p);
		}
		panel_9.setBounds(100,100,100,100);
		panel_9.add(table);
		
		JPanel panel = new JPanel();
		frmVinoteca.getContentPane().add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		JButton btnFactura = new JButton("Confirmar compra");
		panel.add(btnFactura, BorderLayout.EAST);
		
		JButton btnInforme = new JButton("Informe");
		panel.add(btnInforme, BorderLayout.WEST);
		
		//tabla.setVisible(false);
		btnInforme.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				panel_9.add(tabla);
				Dimension d = frmVinoteca.getSize();
				frmVinoteca.setSize(1000,500);
				try {
				
				
					tabla.print();
				} catch (PrinterException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				frmVinoteca.setSize(d);
				tabla.setVisible(false);
			}
		});
		ArrayList<JButton> botones = new ArrayList<JButton>();
		for(Producto p:lista) {
			JButton b = new JButton(p.getNombre());
			
			ImageIcon imagenB=new ImageIcon(p.getImagen());
			Image imagen1B = imagenB.getImage();
			imagen1B = imagen1B.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
			imagenB.setImage(imagen1B);
			b.setIcon(imagenB);
			switch (p.getTipo()) {
			case "Vino":
				Vino.add(b);
			
				break;
			case "Tabaco":
				Tabaco.add(b);
				break;
			case "Caf�":
				Cafe.add(b);
				break;
			case "Refrescos":
				Refrescos.add(b);
				break;
			case "Licores":
				Licores.add(b);
				break;
			case "Conservas":
				Conservas.add(b);
				break;
			case "Cocina":
				Cocina.add(b);
				break;
			case "Helados":
				Helados.add(b);
				break;
			

			default:
				break;
			}botones.add(b);
			
			
		}
		int i = 0;
		for (JButton jButton : botones) {
			
			jButton.addActionListener(new PulsarPr(lista.get(i)));
			i++;
		}
		btnFactura.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Total t = new Total(factura,usu.getId());
			}
		});
		mod.addRow(columnas);
		frmVinoteca.setVisible(true);
		
	}
	class PulsarPr implements ActionListener{
		Producto p;
		public PulsarPr() {
			
		}
		public PulsarPr(Producto p) {
			this.p = p;
		}
		
		public void actionPerformed(ActionEvent e) {
			
			String i = JOptionPane.showInputDialog("Introduce la cantidad de productos que desea");
			String pr[] = {i,p.getNombre(),p.getPrecio()*Integer.parseInt(i)+""};
			if(factura.isEmpty()) {
				p.setCantidad(Integer.parseInt(i));
				mod.addRow(pr);
				factura.add(p);
			}else {
				int popo = 1;
				boolean existe = false;
				
				for (Producto p2 : factura) {
					if (p2.getNombre().equalsIgnoreCase(p.getNombre())) {
						
						p.setCantidad(p2.getCantidad()+Integer.parseInt(i));
						int j = p.getCantidad();
						double pre = p.getPrecio();
						pre *= j;
						mod.setValueAt(j, popo, 0);
						mod.setValueAt(pre, popo, 2);
						existe = true;
						break;
					}
					//System.out.println();
					
						popo++;
				
				}
				if (existe ==false) {
					p.setCantidad(Integer.parseInt(i));
					mod.addRow(pr);
					factura.add(p);
					existe = true;
				}
			
			popo = 1;
			
			}
			
			
			
		}
		
	}

}
