package vinos;

public class Carro {
private int id;
private int idP;
private int cantidad;

public Carro() {
	super();
}
public Carro(int id, int idP, int cantidad) {
	super();
	this.id = id;
	this.idP = idP;
	this.cantidad = cantidad;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getIdP() {
	return idP;
}
public void setIdP(int idP) {
	this.idP = idP;
}
public int getCantidad() {
	return cantidad;
}
public void setCantidad(int cantidad) {
	this.cantidad = cantidad;
}
}
