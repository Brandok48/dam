package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the proyectos database table.
 * 
 */
@Entity
@Table(name="proyectos")
@NamedQuery(name="Proyecto.findAll", query="SELECT p FROM Proyecto p")
public class Proyecto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idP;

	private String nombrep;

	//bi-directional many-to-many association to Empleado
	@ManyToMany
	@JoinTable(
		name="emp_pr"
		, joinColumns={
			@JoinColumn(name="idPr")
			}
		, inverseJoinColumns={
			@JoinColumn(name="idEmp")
			}
		)
	private List<Empleado> empleados;

	public Proyecto() {
	}

	public int getIdP() {
		return this.idP;
	}

	public void setIdP(int idP) {
		this.idP = idP;
	}

	public String getNombrep() {
		return this.nombrep;
	}

	public void setNombrep(String nombrep) {
		this.nombrep = nombrep;
	}

	public List<Empleado> getEmpleados() {
		return this.empleados;
	}

	public void setEmpleados(List<Empleado> empleados) {
		this.empleados = empleados;
	}
	public void anadirEmpleado(Empleado emp) {
		empleados.add(emp);
	}

}