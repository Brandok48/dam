import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Empleado;
import model.Proyecto;

public class Main {
	static EntityManager entityManager;
	static EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("JPA3");
	public static void main(String[] args) {
		entityManager = crearManager();
		Empleado empleado = entityManager.find(Empleado.class, 14);
		Proyecto proyecto = entityManager.find(Proyecto.class, 1);
		entityManager.getTransaction().begin();
		proyecto.anadirEmpleado(empleado);
		//empleado =  entityManager.find(Empleado.class, 69);
		//proyecto = entityManager.find(Proyecto.class, 2);
		//proyecto.anadirEmpleado(empleado);
		entityManager.getTransaction().commit();
		
	}
	
	
	public static EntityManager crearManager() {
		
		entityManager = emfactory.createEntityManager();
		return entityManager;
	}
}
