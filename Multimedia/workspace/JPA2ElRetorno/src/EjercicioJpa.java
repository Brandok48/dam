
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Departamento;
import model.Empleado;



public class EjercicioJpa {
	static EntityManager entityManager;
	static Query query;
	public static EntityManager crearManager() {
		EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("JPA2ElRetorno");
		entityManager = emfactory.createEntityManager();
		return entityManager;
	}
	
	public static Query consultar(String q) {
		
		query  = entityManager.createQuery(q);
		return query;
	}
@SuppressWarnings("unchecked")
public static void main(String[]args) {
		
		Scanner sc = new Scanner(System.in);
		
		entityManager = crearManager();
		
		
		 int opcion = 1;
			while(opcion!=0) {
				try {
					//menu principal
					System.out.println("+----------------------------------------------------+");
					System.out.println("| que quieres hacer?                                 |\n"
									 + "|  1. Listar empleados                               |\n"
									 + "|  2. Listar apellidos de empleados                  |\n"
									 + "|  3. Obtener empleado con mayor salario             |\n"
									 + "|  4. Realizar b�squeda de empleados por apellido    |\n"
									 + "|  5. Buscar empleados por departamento              |\n"
									 + "|  6. Buscar empleados por departamentos             |\n"
									 + "|  0. Terminar                                       |");
					System.out.println("+----------------------------------------------------+");
					opcion = sc.nextInt();
					
						switch (opcion) {
							case 1:
								query = entityManager.createNamedQuery("Empleado.findAll");
								List<Empleado> popo = query.getResultList();
								
								query = consultar("SELECT e FROM Empleado e");
								@SuppressWarnings("unused") List<Empleado> listaE = query.getResultList();
								for(Empleado s:popo) {
									query = consultar("SELECT d.dnombre FROM Departamento d where d.deptNo like '"+s.getDepartamento().getDeptNo()+"'");
									String pepe = (String) query.getSingleResult();
									System.out.println(s.getEmpNo()+" "+s.getApellido()+" "+s.getSalario()+" "+pepe);
								}
								break;
							case 2:
								
								query = consultar("SELECT e.apellido FROM Empleado e order by e.apellido DESC");
								
								List<String> lista = query.getResultList();
								for(String s:lista) {
									System.out.println(s);
								}
								break;
							case 3:
								query = consultar("SELECT e FROM Empleado e where e.salario like (SELECT max(e.salario) FROM Empleado e)");
								Empleado em =  (Empleado) query.getSingleResult();
								
								System.out.println(em.getEmpNo()+" "+em.getApellido()+" "+em.getSalario()+" "+em.getDepartamento().getDeptNo());
								
								
								break;
							case 4:
								sc.nextLine();
								query = consultar("SELECT e FROM Empleado e");
								List<Empleado> listaE2 = query.getResultList();
								for(Empleado s:listaE2) {
									System.out.println(s.getApellido());
								}
								System.out.println("introduce el apellido del empleado deseado");
								String ape = sc.nextLine();
								query = consultar("SELECT e FROM Empleado e where e.apellido like :apellido");
								query.setParameter("apellido", ape);
								Empleado e = (Empleado) query.getSingleResult();
								
								query = consultar("SELECT d.dnombre FROM Departamento d where d.deptNo like '"+e.getDepartamento().getDeptNo()+"'");
								String pepe = (String) query.getSingleResult();
								
								System.out.println("id: "+e.getEmpNo()+"\nApellido: "+e.getApellido()+"\nSalario: "+e.getSalario()+"\nDepartamento: "+pepe);
								
								
								break;
							case 5:
								sc.nextLine();
								query = consultar("SELECT e FROM Departamento e");
								List<Departamento> listaD = query.getResultList();
								for(Departamento s:listaD) {
									System.out.println(s.getDnombre());
								}
								System.out.println("introduce el nombre del departamento deseado");
								String dep = sc.nextLine();
								query = consultar("select d from Departamento d where d.dnombre like :departamento");
								query.setParameter("departamento", dep);
								Departamento d = (Departamento) query.getSingleResult();
								query = consultar("SELECT e FROM Empleado e where e.deptNo like :idDept and e.salario > :salario");
								query.setParameter("idDept", d.getDeptNo());
								query.setParameter("salario", 1000);
								
								
								List<Empleado>listae =  query.getResultList();
								
								
								for(Empleado e1:listae)
								System.out.println("id: "+e1.getEmpNo()+"\nApellido: "+e1.getApellido()+"\nSalario: "+e1.getSalario()+"\nDepartamento: "+d.getDnombre());
								
								
								break;
							case 6:
								sc.nextLine();
								query = consultar("SELECT e FROM Departamento e");
								List<Departamento> listaD5 = query.getResultList();
								for(Departamento s:listaD5) {
									System.out.println(s.getDnombre()+"\nNumero de departamento:"+s.getDeptNo());
								}
								
								
								ArrayList<Integer> nums = new ArrayList<Integer>();
								boolean terminar = false;
								while (!terminar) {
									System.out.println("Inserta el numero de departamento, para terminar pulse 0:");
									int i = sc.nextInt();
									if(i==0)
										terminar=true;
									else
									nums.add(i);
								}
							
								query = consultar("SELECT e FROM Empleado e where e.deptNo in :idDept");
								query.setParameter("idDept", nums);
								
								
								
								List<Empleado>listae1 =  query.getResultList();
								
								
								for(Empleado e1:listae1) {
									query = consultar("SELECT d.dnombre FROM Departamento d where d.deptNo like '"+e1.getDepartamento().getDeptNo()+"'");
									String pepe2 = (String) query.getSingleResult();
								System.out.println("id: "+e1.getEmpNo()+"\nApellido: "+e1.getApellido()+"\nSalario: "+e1.getSalario()+"\nDepartamento: "+pepe2);
								
								}
								break;
							case 7:
								query = consultar("Select d from Departamento d");
								List<Departamento> departamentos= query.getResultList();
								for (Departamento departamento : departamentos) {
									System.out.println("******Departamento******\n"+departamento.getDeptNo()+"\n"+departamento.getDnombre()+"\n");
									List<Empleado> empleados= (List<Empleado>) departamento.getEmpleados();
									System.out.println("******Empleados*********");
									for (Empleado emp : empleados) {
										System.out.println(emp.getEmpNo()+"\n"+emp.getApellido()+"\n"+emp.getSalario()+"\n"+departamento.getDnombre()+"\n------------------------");
									}
								}
								break;
							case 8:
								query = consultar("Select e from Empleado e");
								List<Empleado> empleados= query.getResultList();
									System.out.println("******Empleados*********");
									for (Empleado emp : empleados) {
										System.out.println(emp.getEmpNo()+"\n"+emp.getApellido()+"\n"+emp.getSalario()+"\n"+emp.getDepartamento().getDnombre()+"\n------------------------");
									}
								break;
							case 0:
								break;
							default:
								
								System.out.println("por favor introduzca un numero del 0 al 6");
					
				}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			
		sc.close();
	}

}
