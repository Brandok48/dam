import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import model.Categoria;
import model.Departamento;
import model.DeptCategoria;
import model.Empleado;

public class EjercicioCategorias {
	static EntityManager entityManager;
	static EntityManagerFactory emfactory = Persistence.createEntityManagerFactory("JPA2ElRetorno");;
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int opcion = 1;
		while(opcion!=0) {
			
				try {
					//menu principal
					System.out.println("+----------------------------------------------------+");
					System.out.println("| que quieres hacer?                                 |\n"
									 + "|  1. Listar Empleados y categorias                  |\n"
									 + "|  2. Crear Categoria/s                              |\n"
									 + "|  3. Asignar categorias a empleados                 |\n"
									 + "|  4. Ver Categoria de departamentos                 |\n"
									 + "|  0. Terminar                                       |");
					System.out.println("+----------------------------------------------------+");
					opcion = sc.nextInt();
					
						switch (opcion) {
							case 1:
								sc.nextLine();
								listarEmpleados();
								break;
							case 2:
								sc.nextLine();
								crearCategoria(sc);
								break;
							case 3:
								sc.nextLine();
								asignarCategoria(sc);
								break;
							case 4:
								sc.nextLine();
								contar(sc);
								break;
							case 0:
								break;
							default:
								
								System.out.println("por favor introduzca un numero del 0 al 4");
					
					}
					}catch(Exception e) {
						System.out.println(e);
					}
			}
		sc.close();
		
	}
	@SuppressWarnings("all")
	public static void contar(Scanner sc) {
		entityManager = crearManager();
		List<Departamento> depts = entityManager.createNamedQuery("Departamento.findAll").getResultList();
		System.out.println("*****DEPARTAMENTOS*****");
		for (Departamento dep : depts) {
			System.out.println(dep.getDeptNo()+" "+dep.getDnombre());
		}
		System.out.println("***********************");
		System.out.println("selecciona el id del departamento");
		int numDept = sc.nextInt();
		List<Categoria> cate = entityManager.createNamedQuery("Categoria.findAll").getResultList();
		System.out.println("*****DEPARTAMENTOS*****");
		for (Categoria cat : cate) {
			System.out.println(cat.getId() + " " + cat.getNombre());
		}
		System.out.println("selecciona el id de la categor�a");
		int numCat = sc.nextInt();
		Query query = entityManager.createQuery(
				"SELECT NEW model.DeptCategoria (e.departamento.dnombre, e.categoria.nombre, COUNT(e.empNo)) FROM Empleado e WHERE e.departamento.deptNo=:numDept AND e.categoria.id=:numCat");
		query.setParameter("numDept", numDept);
		query.setParameter("numCat", numCat);
		DeptCategoria deptCat = (DeptCategoria) query.getSingleResult();
		System.out.println("************* N� EMPLEADOS DEPT/CAT *********");

		System.out.println("Departamento NOMBRE = " + deptCat.getDepartamento());
		System.out.println("Categoria NOMBRE = " + deptCat.getNombreCat());
		System.out.println("N�mero empleados = " + (int) deptCat.getNumEmp());

		
		
	}
	@SuppressWarnings("unchecked")
	public static void listarEmpleados() {
		
		List<Categoria> cat = entityManager.createNamedQuery("Categoria.findAll").getResultList();
		for (Categoria categoria : cat) {
			List<Empleado> empleados = categoria.getEmpleados();
			System.out.println("******CATEGORIA*******");
			System.out.println(categoria.getId()+" "+categoria.getNombre()+"\n");
			System.out.println("******EMPLEADOS*******");
			for (Empleado emp : empleados) {
				System.out.println(emp.toString());
			}
			
		}
		entityManager.close();

	}
	@SuppressWarnings("unchecked")
	public static void crearCategoria(Scanner sc) {
		entityManager = crearManager();
		List<Categoria> cat = entityManager.createNamedQuery("Categoria.findAll").getResultList();
		for (Categoria categoria : cat) {
			System.out.println(categoria.getId()+" "+categoria.getNombre());
		}
		Categoria cate = new Categoria();
		System.out.println("Introduce una categoria");
		entityManager.getTransaction().begin();
		cate.setNombre(sc.nextLine());
		entityManager.persist(cate);
		
		entityManager.getTransaction().commit();
		entityManager.close();

	}
	public static void asignarCategoria(Scanner sc) {
		
			listarEmpleados();
			entityManager = crearManager();
			System.out.println("introduce el id del empleado");
			Empleado empleado = entityManager.find(Empleado.class, sc.nextInt());
			sc.nextLine();
		if (empleado!=null) {
			entityManager.getTransaction().begin();
			System.out.println("introduce el id de la categoria que deseas asignar");
			Categoria cate = entityManager.find(Categoria.class, sc.nextInt());
			Categoria cate2 = empleado.getCategoria();
			empleado.setCategoria(cate);
			entityManager.refresh(cate);
			entityManager.refresh(cate2);
			entityManager.getTransaction().commit();
			
			
		}
		entityManager.close();
	}
	public static EntityManager crearManager() {
		
		entityManager = emfactory.createEntityManager();
		return entityManager;
	}
}
