package model;

public class DeptCategoria {
private String departamento;
private String nombreCat;
private long numEmp;


public DeptCategoria() {
	super();
}

public DeptCategoria(String departamento, String nombreCat, long numEmp) {
	super();
	this.departamento = departamento;
	this.nombreCat = nombreCat;
	this.numEmp = numEmp;
}

public String getDepartamento() {
	return departamento;
}
public void setDepartamento(String departamento) {
	this.departamento = departamento;
}
public String getNombreCat() {
	return nombreCat;
}
public void setNombreCat(String nombreCat) {
	this.nombreCat = nombreCat;
}
public long getNumEmp() {
	return numEmp;
}
public void setNumEmp(long numEmp) {
	this.numEmp = numEmp;
}

}
