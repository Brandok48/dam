
import React, {
  Component
}from 'react'
import {
  View,
  Text,
  Button,
  TextInput,
  Alert,
}from 'react-native'

class App extends Component{
  state = {
    contador:0,
    saludo:"",
    mensaje:"",
    kmh:0,
    nudos:0
  }
  
  render(){
    return (
      <View>
        <TextInput placeholder ="Escribe tu nombre" value={this.state.saludo} 
        onChangeText={
          (texto) =>{
            this.setState({saludo:texto})
          }
        }></TextInput>
        
        <Button title={"Suma"} 
        onPress={
         () => {
           var c = this.state.contador;
           var s = this.state.saludo;
           c++;
          this.setState({contador : c});
          this.setState({mensaje:"Hola " + s})
          }

        }/>
        <Text>
          {this.state.contador}
        </Text>
        <Text>
          {this.state.mensaje}
        </Text>
        
        <TextInput placeholder ="Nudos" value={this.state.nudos}
        onChangeText={
          (texto) =>{
            this.setState({nudos:texto})
          }
        }></TextInput>
        <TextInput placeholder ="Km/H" value ={this.state.kmh}
        onChangeText={
          (texto) =>{
            this.setState({kmh:texto})
          }
        }></TextInput>
        
        <Button title={"Nudos a Km/H"} 
          onPress={
          () => {
            var n = this.state.nudos*1.852;
            this.setState({kmh:n.toString()});
            }

          }/>

        <Button title={"Km/H a Nudos"} 
          onPress={
          () => {
            var k = this.state.kmh/1.852;
            this.setState({nudos:k.toString()});
            }

          }/>
      </View>
    );
  }
}


export default App;
