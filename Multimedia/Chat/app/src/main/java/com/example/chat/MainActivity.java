package com.example.chat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {

    TextView tv;
    EditText et;
    Button bt;
    Conexion conn;
    String tx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et = findViewById(R.id.mess);
        bt = findViewById(R.id.send);

       /*final Handler principal = new Handler(){
           @Override
           public void handleMessage(Message inputMessage) {
              super.handleMessage(inputMessage);

              String msg = inputMessage.getData().getString("MENSAJE");
              write(msg);

           }
       };*/

       /* HiloSecundario h =new HiloSecundario();
        h.execute();*/
        Handler principal = new Handler();
        tv = findViewById(R.id.mensaje);
        conn =new Conexion(principal,tv);
        conn.execute();
        //conn.recibirMensaje();
      /*  HiloUno uno =  new HiloUno("uno");
        uno.start();
        Looper looperuno = uno.getLooper();
        final Handler handler = new Handler(looperuno);*/

        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            tx = et.getText().toString();

                           conn.escribir(tx);
                           et.setText("");

            }
        });
        //Socket s = new Socket("10.0.2.2", 4848);
       /* HiloDos dos = new HiloDos("dos");
        dos.start();



        Looper looperdos = dos.getLooper();
        Handler handlerdos = new Handler(looperdos);

        //TextView t = findViewById(R.id.mensaje);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });
        handler.post(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    Log.e("mensajeHiloUno", "Soy hilo 1");

                   // t.append("\nSoy hilo 1");
                }
            }

        });

        handlerdos.post(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    Log.e("mensajeHiloDos", "Soy hilo 2");

                  //  t.append("\nSoy hilo 2");
                }
            }
        });


        handlerdos.post(new Runnable() {
            @Override
            public void run() {

                for(int i= 0;i<500;i++) {
                    Message msg = principal.obtainMessage();
                    Bundle bundle = new Bundle();
                    bundle.putString("MENSAJE", "2");
                    msg.setData(bundle);
                    principal.sendMessage(msg);
                }
                principal.post(new Runnable() {

                    @Override
                    public void run() {
                        //for(int i= 0;i<20;i++)

                        write("2");
                    }
                });
            }
        });*/


    }
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        tv.setText(savedInstanceState.getString("texto"));
    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    @Override
    public void onSaveInstanceState(Bundle outState) {
       // conn.desconectar();
        outState.putString("texto", tv.getText().toString());

        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }
    public void write(String s){
        tv = findViewById(R.id.mensaje);
        tv.append("\n"+s);
    }

}
