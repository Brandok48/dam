package xmlSax;
/**
 * Model class. Its instances will be populated using SAX parser.
 * */
public class Coche
{
    //XML attribute id
    private int id;
    //XML element fisrtName
    private String modelo;
    //XML element lastName
    private String marca;
    
    private String cilindrada;
 
 
    @Override
    public String toString() {
        return this.id + ":" + this.modelo +":" + this.marca +  ":" +this.cilindrada ;
    }



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getModelo() {
		return modelo;
	}


	public void setModelo(String modelo) {
		this.modelo = modelo;
	}


	public String getMarca() {
		return marca;
	}


	public void setMarca(String marca) {
		this.marca = marca;
	}


	public String getCilindrada() {
		return cilindrada;
	}


	public void setCilindrada(String cilindrada) {
		this.cilindrada = cilindrada;
	}
}