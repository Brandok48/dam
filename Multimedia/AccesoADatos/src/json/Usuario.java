package json;

public class Usuario {
	private double id;
	private String nombre;
	private int edad;
	private String poblacion;
	public Usuario(int id, String nombre, int edad, String poblacion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.edad = edad;
		this.poblacion = poblacion;
	}
	public Usuario() {
		super();
	}
	public double getId() {
		return id;
	}
	public void setId(double d) {
		this.id = d;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getPoblacion() {
		return poblacion;
	}
	public void setPoblacion(String poblacion) {
		this.poblacion = poblacion;
	}
}
