package json;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileWriter;


 
public class EjercicioJSON {
	public static Object devolverJSON() {
		JSONParser parser = new JSONParser();
		Object obj = null;
		try {
			obj = parser.parse(new FileReader(
			        "usuarios.json"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}
	//metodo que devuelve la cantidad de usuarios si b es false, y imprime la lista si es true
	public static int verUsuarios(boolean b) {
		
		int i = 0;
		try {
			 
            
 
            JSONArray jsonObject = (JSONArray) devolverJSON();
            
    		@SuppressWarnings("unchecked")
			Iterator<Object> iterator = jsonObject.iterator();
            while (iterator.hasNext()) {
            	i++;
            	JSONObject usuario = (JSONObject) iterator.next();
            	if (b)
                System.out.println("id: "+usuario.get("id")+"\nnombre: "+usuario.get("nombre").toString()+"\nEdad: "+usuario.get("edad").toString()+"\nPoblacion: "+usuario.get("poblacion").toString()+"\n");
            }
 
 
        } catch (Exception e) {
            e.printStackTrace();
        }
		return i;
	}
	@SuppressWarnings("unchecked")
	public static void anadirUsu(Scanner sc) {
		boolean error = true;
		
		Usuario usu = new Usuario();
		JSONObject user = new JSONObject();
		JSONArray obj = (JSONArray) devolverJSON();
		while(error){
			try {
				usu.setId((int) Math.floor(Math.random()*1000));
				String id = usu.getId()+"";
				id = id.substring(0, id.indexOf("."));
				user.put("id", id);
				sc.nextLine();
				System.out.println("introduce el nombre: ");
				usu.setNombre(sc.nextLine());
				user.put("nombre", usu.getNombre());
				
				System.out.println("introduce la poblaci�n: ");
				usu.setPoblacion(sc.nextLine());
				user.put("poblacion", usu.getPoblacion());
				System.out.println("introduce la edad: ");
				usu.setEdad(sc.nextInt());
				user.put("edad", usu.getEdad());
				
				error = false;
				
				obj.add(user);
				
				FileWriter fw = new FileWriter("usuarios.json");
				fw.write(obj.toString());
				fw.close();
				
			}catch(Exception e) {
				error = true;
				System.out.println("Ha habido un error vuelva a introducir el usuario\n");
				sc.next();
			}
		} 
	}
	public static int buscarUsu(int id) {
		int pos = 0;
		try {
			 
            
			 
            JSONArray jsonObject = (JSONArray) devolverJSON();
            
    		@SuppressWarnings("unchecked")
			Iterator<Object> iterator = jsonObject.iterator();
            while (iterator.hasNext()) {
            	
            	JSONObject usuario = (JSONObject) iterator.next();
            	if (id != Integer.parseInt(usuario.get("id").toString()))
               pos++;
            	else
            		break;
            }
 
 
        } catch (Exception e) {
            e.printStackTrace();
        }
		
		return pos;
		
	}
	public static void eliminarUsu(Scanner sc) {
		boolean err = true;
		int id;
		
		JSONArray obj = (JSONArray) devolverJSON();
		FileWriter fw;
				while(err) {
				
				System.out.println("introduce el id del usuario a eliminar");
				id = sc.nextInt();
				try {
					obj.remove(buscarUsu(id));
					fw = new FileWriter("usuarios.json");
					fw.write(obj.toString());
					fw.close();
					err = false;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					
					System.out.println("ese usuario no existe");
					err = true;
				}
				}
		
	}
	public static void verUsu(Scanner sc) {
		boolean err = true;
		int id;
		JSONArray obj = (JSONArray) devolverJSON();
				while(err) {
				
				System.out.println("introduce el id del usuario a visualizar");
				id = sc.nextInt();
				try {
					JSONObject user = (JSONObject) obj.get(buscarUsu(id));
					System.out.println("id: "+user.get("id")+"\nNombre: "+user.get("nombre")+"\nEdad: "+user.get("edad")+"\nPoblaci�n: "+user.get("poblacion"));
					err = false;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					
					System.out.println("ese usuario no existe");
					err = true;
				}
				}
		
	}
	@SuppressWarnings("unchecked")
	public static void modUsu(Scanner sc) {
		
		boolean err = true;
		int id;
		JSONArray obj = (JSONArray) devolverJSON();
				while(err) {
				
				System.out.println("introduce el id del usuario a modificar");
				
				try {
					id = sc.nextInt();
					JSONObject user = (JSONObject) obj.get(buscarUsu(id));
					sc.nextLine();
					System.out.println("introduce el nuevo nombre");
					user.put("nombre", sc.nextLine());
					System.out.println("introduce la nueva poblaci�n");
					user.put("poblacion",sc.nextLine());
					System.out.println("introduce la neva edad");
					user.put("edad", sc.nextInt());
					
					FileWriter fw = new FileWriter("usuarios.json");

					fw.write(obj.toString());
					fw.close();
					err = false;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					
					System.out.println("ese usuario no existe");
					sc.next();
					err = true;
				}
				}
	}
   
    public static void main(String[] args) {
    	Scanner sc = new Scanner(System.in);
        
        int opcion = 1;
		while(opcion!=0) {
			try {
				System.out.println("+------------------------+");
				System.out.println("| que quieres hacer?     |\n"
								 + "|  1. Cargar usuarios    |\n"
								 + "|  2. Mostrar usuarios   |\n"
								 + "|  3. A�adir usuario     |\n"
								 + "|  4. Eliminar usuario   |\n"
								 + "|  5. Consultar usuario  |\n"
								 + "|  6. Modificar usuario  |\n"
								 + "|  0. Terminar           |");
				System.out.println("+------------------------+");
				opcion = sc.nextInt();
					switch (opcion) {
						case 1:
							System.out.println("se han cargado "+verUsuarios(false)+" Usuarios");
							break;
						case 2:
							verUsuarios(true);
							break;
						case 3:
							
							anadirUsu(sc);
							
							break;
						case 4:
							
							eliminarUsu(sc);
							break;
						case 5:
							verUsu(sc);
							break;
						case 6:
							modUsu(sc);
							break;
						case 0:
							
							break;
						default:
							if(opcion<0||opcion>6)
							System.out.println("por favor introduzca un numero del 0 al 6");
				
			}
			}catch(Exception e) {
				System.out.println("Por favor introduzca un numero del 0 al 6 "+e);
			}finally {
				sc.nextLine();
			}
		}
		
		sc.close();
    }
    
}