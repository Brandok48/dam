package inputOutputStream;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class Input {
	public static void main (String[] args) throws IOException,ClassNotFoundException {
	 Coche coche; //defino variable Coche
	 File fichero = new File("C:\\Users\\2DAM3\\Documents\\DAM/pepe.txt");
	 FileInputStream filein = new FileInputStream (fichero); //entrada
	 //conecta el flujo de bytes al flujo de datos
	 ObjectInputStream datIS = new ObjectInputStream(filein);
	 try {
	 while (true) { //lectura del fichero
	 coche = (Coche) datIS.readObject(); //leer una Coche
	 System.out.println("Marca: " + coche.getMarca() + "\nMatricula: "
	+ coche.getMatricula());
	 }
	 }catch (EOFException eo) {}
	 datIS.close(); //cerrar stream de entrada
	 }

}
