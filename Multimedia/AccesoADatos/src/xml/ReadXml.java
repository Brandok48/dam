package xml;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.*;
public class ReadXml {
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		//Get Document Builder
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		 
		//Build Document
		Document document = builder.parse(new File("C:\\Users\\2DAM3\\Documents\\dam\\Acceso a datos/concesionario.xml"));
		 
		//Normalize the XML Structure; It's just too important !!
		document.getDocumentElement().normalize();
		 
		//Here comes the root node
		Element root = document.getDocumentElement();
		System.out.println(root.getNodeName());
		 
		//Get all cars
		NodeList nList = document.getElementsByTagName("coche");
		System.out.println("============================");
		 
		for (int temp = 0; temp < nList.getLength(); temp++)
		{
		 Node node = nList.item(temp);
		 System.out.println("");    //Just a separator
		 if (node.getNodeType() == Node.ELEMENT_NODE)
		 {
		    //Print each car's detail
		    Element eElement = (Element) node;
		    System.out.println("coche id : "    + eElement.getAttribute("id"));
		    System.out.println("Marca : "  + eElement.getElementsByTagName("marca").item(0).getTextContent());
		    System.out.println("Modelo : "   + eElement.getElementsByTagName("modelo").item(0).getTextContent());
		    System.out.println("Cilindrada : "    + eElement.getElementsByTagName("cilindrada").item(0).getTextContent());
		 }
		}
		Element coche = document.createElement("coche");
		Element marca = document.createElement("marca");
		Element modelo = document.createElement("modelo");
		Element cilindrada = document.createElement("cilindrada");
		marca.setTextContent("pepe");
		modelo.setTextContent("ho");
		modelo.setTextContent("12");
		coche.appendChild(marca);
		coche.appendChild(modelo);
		coche.appendChild(cilindrada);
		root.appendChild(coche);
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(root);
				StreamResult result = new StreamResult(new File("C:\\Users\\2DAM3\\Documents\\dam\\Acceso a datos/concesionario.xml"));
				transformer.transform(source, result);
	
	}

}
