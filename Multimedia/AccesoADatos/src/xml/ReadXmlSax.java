package xml;

import java.util.ArrayList;
import java.util.Stack;
 
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
 
public class ReadXmlSax extends DefaultHandler
{
    //This is the list which shall be populated while parsing the XML.
    private ArrayList CocheList = new ArrayList();
 
    //As we read any XML element we will push that in this stack
    private Stack elementStack = new Stack();
 
    //As we complete one Coche block in XML, we will push the Coche instance in CocheList
    private Stack objectStack = new Stack();
 
    public void startDocument() throws SAXException
    {
        //System.out.println("start of the document   : ");
    }
 
    public void endDocument() throws SAXException
    {
        //System.out.println("end of the document document     : ");
    }
 
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException
    {
        //Push it in element stack
        this.elementStack.push(qName);
 
        //If this is start of 'Coche' element then prepare a new Coche instance and push it in object stack
        if ("Coche".equals(qName))
        {
            //New Coche instance
            Coche Coche = new Coche();
 
            //Set all required attributes in any XML element here itself
            if(attributes != null && attributes.getLength() == 1)
            {
                Coche.setId(Integer.parseInt(attributes.getValue(0)));
            }
            this.objectStack.push(Coche);
        }
    }
 
    public void endElement(String uri, String localName, String qName) throws SAXException
    {
        //Remove last added  element
        this.elementStack.pop();
 
        //Coche instance has been constructed so pop it from object stack and push in CocheList
        if ("Coche".equals(qName))
        {
            Coche object = (Coche) this.objectStack.pop();
            this.CocheList.add(object);
        }
    }
 
    /**
     * This will be called everytime parser encounter a value node
     * */
    public void characters(char[] ch, int start, int length) throws SAXException
    {
        String value = new String(ch, start, length).trim();
 
        if (value.length() == 0)
        {
            return; // ignore white space
        }
 
        //handle the value based on to which element it belongs
        if ("cilindrada".equals(currentElement()))
        {
            Coche Coche = (Coche) this.objectStack.peek();
            Coche.setCilindrada(value);
        }
        else if ("lastName".equals(currentElement()))
        {
            Coche Coche = (Coche) this.objectStack.peek();
            Coche.setMarca(value);
        }
    }
 
    /**
     * Utility method for getting the current element in processing
     * */
    private String currentElement()
    {
        return (String) this.elementStack.peek();
    }
 
    //Accessor for CocheList object
    public ArrayList getCoches()
    {
        return CocheList;
    }
}