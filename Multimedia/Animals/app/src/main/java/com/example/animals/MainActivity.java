package com.example.animals;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final MediaPlayer[] sounds = {MediaPlayer.create(this,R.raw.dog),
                MediaPlayer.create(this,R.raw.cat_noise),
                MediaPlayer.create(this,R.raw.rooster),
                MediaPlayer.create(this,R.raw.donkey),
                MediaPlayer.create(this,R.raw.arriba),
                MediaPlayer.create(this,R.raw.pegleg),
                MediaPlayer.create(this,R.raw.bruh),
                MediaPlayer.create(this,R.raw.oof)};
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String[] lista =  getResources().getStringArray(R.array.arrayAnimales);
        ArrayAdapter<String> itemsAdapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,lista);
        ListView listView = findViewById(R.id.listaAnimales);
        listView.setAdapter(itemsAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                getResources().getStringArray(R.array.arraySonidosAnimales)[position], Toast.LENGTH_SHORT);
                sounds[position].start();
                toast1.show();
            }
        });
    }

}
