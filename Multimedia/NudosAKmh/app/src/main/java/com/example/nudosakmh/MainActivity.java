package com.example.nudosakmh;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button4);
        final EditText nudos = findViewById(R.id.editText2);
        Button button2 = findViewById(R.id.button6);
        final EditText km = findViewById(R.id.editText3);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float kmh = Float.parseFloat(km.getText().toString());
                kmh/=1.852;
                nudos.setText(""+kmh);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                float nudos2 = Float.parseFloat(nudos.getText().toString());
                nudos2*=1.852;
                km.setText(""+nudos2);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button4:
                // Do something

        }
    }
}
