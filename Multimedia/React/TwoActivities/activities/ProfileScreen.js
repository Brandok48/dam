import React,{Component} from "react";
import {Text,Button,View} from 'react-native';
class ProfileScreen extends Component {
  static navigationOptions = {
    title: 'Welcome',
    boton: ''
  };
  render() {
    name = this.props.navigation.getParam('name');
    const {navigate} = this.props.navigation;
    return (
      <View>
        <Text>This is {name}'s profile</Text>
        <Button
        title="Boton 1"
        onPress={() => navigate('Home', {boton: '1'})}
      />
      <Button
        title="Boton 2"
        onPress={() => navigate('Home', {boton: '2'})}
      />
      </View>  
    );
  }
}
export default ProfileScreen;