import React,{Component} from "react";
import {Button,View,Text} from 'react-native';
class HomeScreen extends Component {
 
  static navigationOptions = {
    title: 'Welcome',
    name:'',
  };
 
  render() {
    boton = this.props.navigation.getParam('boton');
    const {navigate} = this.props.navigation;
    
    return (
      <View>
        
        <Button
        title="Go to Jane's profile"
        onPress={() => navigate('Profile', {name: 'Jane'})}


      />
      <Text>{boton}</Text>
      </View>
      
    );
  }
}
export default HomeScreen;