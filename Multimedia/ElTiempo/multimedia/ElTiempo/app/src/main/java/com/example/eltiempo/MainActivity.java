package com.example.eltiempo;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    ImageView im,im2;
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = findViewById(R.id.ppp);
        im = findViewById(R.id.mapa);
        Handler main = new Handler(){
            @Override
            public void handleMessage (Message msg){
                Bundle bundle = msg.getData();
                String str = bundle.getString("mensaje");
                if(str!= null) {
                    escribir(str);
                }
                String img = bundle.getString("imagen");
                if(img!=null) {
                    Log.e("ojojojo", img);
                    imprimir(img);
                }
            }
        };
        new Descarga(main).execute();
    }
    public void escribir(String str){
        tv.append(str);
    }
    public void imprimir(String img){
        if(im==null){
        byte[] decodificado = Base64.decode(img,Base64.DEFAULT);
        Bitmap imagenfinal = BitmapFactory.decodeByteArray(decodificado,0,decodificado.length);
        im.setImageBitmap(imagenfinal);
        }else{
            byte[] decodificado = Base64.decode(img,Base64.DEFAULT);
            Bitmap imagenfinal = BitmapFactory.decodeByteArray(decodificado,0,decodificado.length);
            im2.setImageBitmap(imagenfinal);
        }
    }
}

