package com.example.rescates;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MyViewModel extends ViewModel {
    private MutableLiveData<Coordenada> coord = new MutableLiveData<Coordenada>();
    public void setCoord(Coordenada c) {
        coord.setValue(c);
    }
    public LiveData<Coordenada> getCoords() {

        return coord;
    }


}