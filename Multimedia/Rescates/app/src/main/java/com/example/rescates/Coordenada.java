package com.example.rescates;

public class Coordenada {


    private int latitud;
    private int longitud;
    public Coordenada(){

    }
    public Coordenada(int x, int y){
    latitud = x;
    longitud = y;
    }
    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }
    public int getLatitud() {
        return latitud;
    }

    public void setLatitud(int latitud) {
        this.latitud = latitud;
    }
}
