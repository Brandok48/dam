package com.example.rescates;


import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.rescates.OneFragment;
import com.example.rescates.R;


public class MainActivity extends AppCompatActivity implements OneFragment.OnFragmentInteractionListener {

    private Button btn1, btn2;
    private TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        tv = findViewById(R.id.coordenadas);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(new OneFragment(), false, "one");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFragment(new MapFragment(), false, "one");
            }
        });
        MyViewModel model = ViewModelProviders.of(this).get(MyViewModel.class);
        model.getCoords().observe(this, new Observer<Coordenada>() {
            @Override
            public void onChanged(Coordenada c) {
                // update UI
                tv.setText("Latitud:"+c.getLatitud()+"\nLongitud:"+c.getLongitud());
            }
        });

    }

    public void addFragment(Fragment fragment, boolean addToBackStack, String tag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.container_frame_back, fragment, tag);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onFragmentInteraction(Coordenada c) {
        tv.setText("Latitud:"+c.getLatitud()+"\nLongitud:"+c.getLongitud());
    }
}