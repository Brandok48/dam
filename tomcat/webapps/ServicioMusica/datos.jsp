<%@page import="clases.Disco"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<jsp:useBean id="artista" scope="request" class="clases.Artista" />
<p>Nombre: <%= artista.getNombre()%></p>
<p>Apellido: <%= artista.getApellido()%></p>
<p>Localidad: <%= artista.getLocalidad()%></p>
<fieldset>
<legend>Lista de discos</legend>
<% for(Disco a: artista.getListaDiscos()){%>

		<p>Titulo: <%= a.getNombre() %></p>
		<p>fecha de publicacion: <%= a.getFecha_publi() %></p>
		<br>
		
		<%}%>
	</fieldset>
</body>
</html>