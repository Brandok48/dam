

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SecureClient {
	public static void main(String[] args) {
		try {
			System.out.println("obteniendo factoria de cliente");
			SSLSocketFactory socketFactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
			System.out.println("Creando el socket Cliente");
			SSLSocket clientSocket = (SSLSocket) socketFactory.createSocket();
			System.out.println("Estableciendo la conexion");
			InetSocketAddress addr = new InetSocketAddress("localhost", 4949);
			clientSocket.connect(addr);
			OutputStream os = clientSocket.getOutputStream();
			System.out.println("Enviando mensaje");
			String mensaje = "Mensaje desde el cliente";
			os.write(mensaje.getBytes());
			System.out.println("Mensaje enviado");
			System.out.println("Cerrando socket");
			clientSocket.close();
			System.out.println("Terminado");
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
