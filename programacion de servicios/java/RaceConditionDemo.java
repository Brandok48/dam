public class RaceConditionDemo extends Thread{
    public static int c = 0;
    String name;
    public RaceConditionDemo(String string) {
        this.name = string;
    }
    public void run(){
        System.out.println("hilo "+this.name+": "+c);
        increment();
        System.out.println("hilo "+this.name+": "+c);
        decrement();
        System.out.println("hilo "+this.name+": "+c);
        
    }

    public void increment() {
        try {
            Thread.sleep(100);
           
        } catch (InterruptedException e) {
            
            e.printStackTrace();
        }
         c++;
    }
    public void decrement(){
         c--;
    }
    public int getValue(){
        return c;
    }
    public static void main(String[]args) throws InterruptedException{
        RaceConditionDemo thread1 = new RaceConditionDemo("#1");
        RaceConditionDemo thread2 = new RaceConditionDemo("#2");
        RaceConditionDemo thread3 = new RaceConditionDemo("#3");
        thread1.start();
        thread2.start();
        thread3.start();
        thread1.join();
        thread2.join();
        thread3.join();
        System.out.println("Resultado final "+c);
        
    }
}