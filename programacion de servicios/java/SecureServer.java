

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

public class SecureServer {
	public static void main(String[] args) {

		try {
			System.out.println("Obteniendo factoria de sockets del servidor");
			SSLServerSocketFactory serverSocketFactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
			System.out.println("Creando socket de servidor");
			SSLServerSocket serverSocket = (SSLServerSocket) serverSocketFactory.createServerSocket();
			System.out.println("Realizando el bind");
			InetSocketAddress addr = new InetSocketAddress("localhost",4949);
			serverSocket.bind(addr);
			System.out.println("aceptando conexiones");
			SSLSocket newSocket = (SSLSocket) serverSocket.accept();
			System.out.println("Conexion recibida");
			InputStream is = newSocket.getInputStream();
			byte[] mensaje = new byte[25];
			is.read(mensaje);
			System.out.println("Mensaje recibido: "+new String(mensaje));
			System.out.println("Cerrando el nuevo socket");
			newSocket.close();
			System.out.println("Cerrando el servidor");
			serverSocket.close();
			System.out.println("terminado");
			
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
}
