import java.util.concurrent.Semaphore;

public class Espagueti{
    public static int[] comiendo = {0,0,0,0,0};
    private static Semaphore[] tenedor = {
        new Semaphore(1)
       ,new Semaphore(1)
       ,new Semaphore(1)
       ,new Semaphore(1)
       ,new Semaphore(1)
    };
    private static Filosofo[] filo = {
        new Filosofo(tenedor[4],tenedor[0],0),
        new Filosofo(tenedor[0],tenedor[1],1),
        new Filosofo(tenedor[1],tenedor[2],2),
        new Filosofo(tenedor[2],tenedor[3],3),
        new Filosofo(tenedor[3],tenedor[4],4)};
    public static void main(String[] args){

        for (int i = 0; i < filo.length; i++) {
             
                filo[i].start();
            
        }
        
        for (int i = 0; i < filo.length; i++) {
        try{
                
                filo[i].join();
            }catch(InterruptedException e){
                e.printStackTrace();
            }
        }
        
    }

}

class Filosofo extends Thread{
    
    private Semaphore der;
    private Semaphore izq;
    private int id;
    private int hasta = 10;
    public Filosofo(Semaphore der,Semaphore izq,int id){
        this.der = der;
        this.izq = izq;
        this.id = id;
    }
    //se comprueba que no hay nadie usando el izquierdo
    public synchronized boolean probarIzq(Semaphore izq){
        return (izq.availablePermits()==1);
    }
    //se comprueba que no hay nadie usando el derecho
    public synchronized boolean probarDer(Semaphore der){
        return (der.availablePermits()==1);
    }
    public synchronized void imprimir(){
        String str = "[";
                            
        for(int j = 0;j<Espagueti.comiendo.length;j++){
            
            if(Espagueti.comiendo[j]==1){
                str+="|x";
            }else if(id==j){
                Espagueti.comiendo[id] = 1;
                str+="|y";
            }
            else{
                str+="|.";
            }
            
            
        }
        str+="|]";
        System.out.println(str);
    }
    public void run(){
        for (int i = this.hasta; i> 0; i--) {
            try{
                Thread.sleep((long)Math.random()*1000+1000);
            }catch(InterruptedException e){
                e.printStackTrace();
            }
            if (probarIzq(izq)){
                try {
                  izq.acquire();
                    
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(probarDer(der)){
                    try {
                        der.acquire();
                        imprimir();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                   
                        try{
                            Thread.sleep((long)Math.random()*1000+1000);
                        }catch(InterruptedException e){
                            e.printStackTrace();
                        }
                            izq.release();
                            Espagueti.comiendo[id] = 0;
                      
                            der.release();
                            Espagueti.comiendo[id] = 0;

            }else{
               
                 izq.release();
               
               
            }
        }
                
             
        }
        

    }
    
}
