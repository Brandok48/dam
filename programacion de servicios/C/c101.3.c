/* my first program in C */
#include <stdio.h>
#include <string.h>

void swap(int *a,int *b)
{
    int f = *a;
    
    *a = *b;
    *b = f;
}

void main()
{
    int a = 7;
    int b = 3;
    printf("antes del cambio a = %d b = %d",a,b);
    swap(&a,&b);
    printf("\ndespues del cambio a = %d b = %d",a,b);
}

