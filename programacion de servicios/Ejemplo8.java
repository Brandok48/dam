import java.io.File;
import java.io.IOException;

public class Ejemplo8{
    public static void main(String[] args)throws IOException{
        ProcessBuilder pb = new ProcessBuilder("bash");
        File fbat = new File("ejemplo8.bat");
        File fout = new File("ejemplo8.txt");
        File ferr = new File("ejemplo8.err");
       
        pb.redirectInput(fbat);
        pb.redirectOutput(fout);
        pb.redirectError(ferr);
        pb.start();
    }
}